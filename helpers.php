<?php
if (!function_exists('redirect')) {
    function redirect($url)
    {
        header("Refresh:0; url=" . $url);
        exit();
    }
}
if (!function_exists('admin_logged_in')) {
    function admin_logged_in($data)
    {
        if (isset($data['admin']['logged_in']) && true == $data['admin']['logged_in']) {
            if (isset($data['admin']['last_activity']) && ((time() - $data['admin']['last_activity']) < 17200 * 2)) {
                if (isset($data['admin']['ip_address']) && get_client_ip() == $data['admin']['ip_address']) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return -2;
            }
        } else {
            return 0;
        }
    }
}
if (!function_exists('generate_password')) {
    function generate_password($length = 6)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        return substr(str_shuffle($chars), 0, $length);
    }
}
if (!function_exists('uri_segment')) {
    function uri_segment($uri, $index)
    {
        $data = explode('/', $uri);
        foreach ($data as $value) {
            if ($value == $index) {
                return 1;
            }
        }
        return 0;
    }
}
if (!function_exists('upload_image')) {
    function upload_image($file, $dir, $path, $sizing)
    {
        $dir = $dir . $path;
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file_name = 'original.' . pathinfo($_FILES[$file]['name'], PATHINFO_EXTENSION);
        $path = $path . '/';
        $dir2 = $dir . '/' . $file_name;
        move_uploaded_file($_FILES[$file]['tmp_name'], $dir2);
        if ($sizing && count($sizing) > 0) {
            foreach ($sizing as $key => $row) {
                $size = ['width' => $row['width'], 'height' => $row['height']];
                $dir3 = $dir . '/' . $key . '.' . pathinfo($_FILES[$file]['name'], PATHINFO_EXTENSION);
                image_resizing($dir2, $dir3, $size);
            }
        }
        $image['ext'] = pathinfo($_FILES[$file]['name'], PATHINFO_EXTENSION);
        $image['file_name'] = $path;
        return $image;
    }
}
if (!function_exists('image_resizing')) {
    function image_resizing($path, $path2, $size)
    {
        $source_properties = getimagesize($path);
        $image_type = $source_properties[2];
        if (IMAGETYPE_JPEG == $image_type) {
            $image_resource_id = imagecreatefromjpeg($path);
        } else if (IMAGETYPE_GIF == $image_type) {
            $image_resource_id = imagecreatefromgif($path);
        } else if (IMAGETYPE_PNG == $image_type) {
            $image_resource_id = imagecreatefrompng($path);
        }
        $target_layer = imagecreatetruecolor($size['width'], $size['height']);
        imagecopyresampled($target_layer, $image_resource_id, 0, 0, 0, 0, $size['width'], $size['height'], $source_properties[0], $source_properties[1]);
        if (IMAGETYPE_JPEG == $image_type) {
            imagejpeg($target_layer, $path2);
        } else if (IMAGETYPE_GIF == $image_type) {
            imagegif($target_layer, $path2);
        } else if (IMAGETYPE_PNG == $image_type) {
            imagepng($target_layer, $path2);
        }
    }
}
if (!function_exists('create_slug')) {
    function create_slug($str)
    {
        $search = ['Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë'];
        $replace = ['s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E'];
        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '', $str);
        $str = str_replace(' ', '-', $str);
        return preg_replace('/\-{2,}/', '-', $str);
    }
}
if (!function_exists('get_client_ip')) {
    function get_client_ip()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_address = $_SERVER['HTTP_CLIENT_IP'];
        } else if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip_address = $_SERVER['REMOTE_ADDR'];
        } else if (isset($_SERVER['HTTP_FORWARDED'])) {
            $ip_address = $_SERVER['HTTP_FORWARDED'];
        } else if (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip_address = 'UNKNOWN';
        }
        return $ip_address;
    }
}
if (!function_exists('encode_url')) {
    function encode_url($num)
    {
        $new_Id = $num + 11;
        $newId = $new_Id * 9;
        $random_string = generateRandomString();
        $random_nums = genRandNums();
        $randNums = genRandNums();
        $randString = genRandString();
        $new_url = $random_string . $randNums . $newId . $random_nums . $randString;
        return $new_url;
    }
}
if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('genRandNums')) {
    function genRandNums($length = 5)
    {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('genRandString')) {
    function genRandString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('decode_url')) {
    function decode_url($encoded_uri)
    {
        if (strlen($encoded_uri) < 33) {
            return false;
        } else {
            $substrig = substr($encoded_uri, 15, -15);
            $substrig1 = ($substrig / 9) - 11;
            return $substrig1;
        }
    }
}
if (!function_exists('encode_serial')) {
    function encode_serial($num)
    {
        $hash = $num * 369852147;
        $serialNum = substr($hash, -9);
        return $serialNum;
    }
}
