<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Edit Sale Detail';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
if (!isset($_GET['s_id']) || $_GET['s_id'] == "") {
    $msg['errors'] = "There might be some errors. Try again later.";
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/sale-reports/index");
} else {
    $id = $_GET['s_id'];
    $data = SaleDetail::find(['conditions' => ['s_id' => $id]]);
    if ($data == "") {
        $msg['errors'] = "No Record Found.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/sale-reports/index");
    }
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Sale Detail
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <form role="form"
                              class="col-md-offset-3 col-md-6 col-md-offset-2 col-md-8 col-md-offset-3 col-md-6"
                              enctype="multipart/form-data"
                              id="activity_validate" method="POST"
                              action="<?= $base_url ?>controllers/user/sales?action=update&s_id=<?= $_GET['s_id'];?>">
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <div class="box-body">
							    <div class="form-group">
                                <label for="s_daily_id">Select Activity</label>
                                      <select class="form-control" required name="s_daily_id" id="s_daily_id">									  
									      <option 
										  value="<?= $data->s_id; ?>"><?= date("d-m-Y",strtotime($data->s_d_entry_date)); ?></option>		  
									 </select>
                                </div>								
                                <div class="form-group">
                                    <label for="s_customer_name">Trading Name</label>
                                    <input required type="text" class="form-control" name="s_customer_name"
                                              id="s_customer_name"
                                              placeholder="Enter Trading Name" value="<?= $data->s_customer_name; ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="s_service">Lead Source</label>
									<select class="form-control" required name="s_service" id="s_service">
                                       <option value="1" <?php echo ($data->s_service==1)?'selected="selected"':''?>>Cold Call</option>
                                       <option value="2" <?php echo ($data->s_service==2)?'selected="selected"':''?>>TeleSales</option>
                                       <option value="3" <?php echo ($data->s_service==3)?'selected="selected"':''?>>Referral</option> 									
                                    </select>									
                                </div>						
                                <div class="form-group">
                                    <label for="s_package">Package ID</label>
                                    <input required type="text"  class="form-control" name="s_package"
                                              id="s_package"
                                              placeholder="Enter Package ID" value="<?= $data->s_package; ?>" />
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
$('#s_daily_id').on('change', function() {
	var value=$(this).val();
	var getDate=$(this).children('option[value='+value+']');
    $("#s_d_entry_date").val(getDate.attr("date"));
});


	
</script>