<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
$author = "";
$keywords = "";
$description = "";
$page_name = "Forgot Password";
if (admin_logged_in($_SESSION) == 1) {
    redirect($base_url . "admin/dashboard");
}
if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = [];
}
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?=ucwords($page_name);?> | <?=ucwords($site_name);?></title>
        <meta name="author" content="<?=ucwords($author);?>">
        <meta name="description" content="<?=ucwords($description);?>">
        <meta name="keywords" content="<?=ucwords($keywords);?>">
        <link rel="icon" href="<?=$base_url?>assets/frontend/images/13.png" sizes="32x32" type="image/png">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?=$base_url;?>assets/admin/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=$base_url;?>assets/admin/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?=$base_url;?>assets/admin/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="<?=$base_url;?>assets/admin/adminlte/css/AdminLTE.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="<?=$base_url;?>admin/login">
            <img src="<?=$base_url;?>payzone-logo.png" alt="<?=ucwords($site_name);?>"/></a>
        </div>
        <div class="login-box-body">
            <p class="login-box-admin_msg">Enter email to reset your password</p>
            <?php require_once $app_path . 'views/errors.php';?>
            <form action="<?=$base_url;?>controllers/admin/admin_login" method="POST" enctype="multipart/form-data"
                  id="form_validate">
                <div class="form-group has-feedback">
                    <input type="email" required class="form-control" name="email" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-offset-6 col-xs-6">
                        <button type="submit" name="forgot_password" class="btn btn-primary btn-block btn-flat">Forgot Password</button>
                    </div>
                </div>
            </form>
            <a href="<?=$base_url;?>admin/login"> <i class="fa fa-arrow-left"></i>&nbsp Back To Login</a>
        </div>
    </div>
    <script src="<?=$base_url;?>assets/admin/jquery/dist/jquery.min.js"></script>
    <script src="<?=$base_url;?>assets/admin/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?=$base_url;?>assets/admin/jquery_validation/dist/jquery.validate.min.js"></script>
    <script src="<?=$base_url;?>assets/admin/jquery_validation/dist/additional-methods.min.js"></script>
    <script>
        $(function () {
            $("#form_validate").validate({
                errorClass: 'error_class',
                validClass: 'valid_class',
            });
            setTimeout(function(){
                $('.alert').fadeOut('2000');
            },3000);
        });
    </script>
    </body>
    </html>
<?php
unset($_SESSION['admin']['msg']);
?>