<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Dashboard';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?= $base_url ?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        <section class="content-header">
            <?php require_once $app_path . 'views/errors.php'; ?>
        </section>
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <?php if ($_SESSION['admin']['type'] == '0') { ?>
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#E3027F; color:white;">
                            <div class="inner">
                                <h3>Add</h3>

                                <p>New User</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/users/add" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#434343; color:white;">
                            <div class="inner">
                                <h3><?php
                                    $month=date('m');
                                    $year=date('Y');
                                    echo $actT=count(Activities::find_by_sql('select * from d where month(d_entry_date)="'.$month.'" and year(d_entry_date)="'.$year.'"  '));
                                    $actD=count(Activities::find_by_sql('select * from d where d_entry_date="'.date('Y-m-d').'" '));
                                    ?></h3>

                                <p>Activities <span style="font-size:10px;">For (<?= $month=date('M Y'); ?>)</span></p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-bar-chart"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/reports/index" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->

                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#B80060; color:white;">
                            <div class="inner">
                                <h3><?php
                                    $month=date('m');
                                    $year=date('Y');
                                    echo $salesT=count(SaleDetail::find_by_sql('select * from sale_detail where month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"  '));
                                    $salesD=count(SaleDetail::find_by_sql('select * from sale_detail where s_d_entry_date="'.date('Y-m-d').'" '));
                                    ?></h3>

                                <p>Sales <span style="font-size:10px;">For (<?= $month=date('M Y'); ?>)</span></p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-ban"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/sale-reports/index" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#676767; color:white;">
                            <div class="inner">
                                <h3>
                                    <?php
                                    $iCount=0;
                                    if($_SESSION['admin']['type']=='0'){
                                        $results = Activities::find_by_sql('SELECT * , CAST(d_entry_date AS DATE) AS d_entry_date FROM d JOIN user ON user.user_id =  d.d_user_id ORDER BY d_entry_date DESC');
                                    }else{
                                        $results = Activities::find_by_sql('SELECT * , CAST(d_entry_date AS DATE) AS d_entry_date FROM d JOIN user ON user.user_id =  d.d_user_id and user.user_id='.$_SESSION["admin"]["id"].' ORDER BY d_entry_date DESC');
                                    }
                                    if (count($results) > 0): ?>

                                        <?php foreach ($results as $key => $row): ?>
                                            <?php
                                            $sales = $row->d_sales;
                                            $sales_detail = SaleDetail::find('all',['conditions'=>['s_daily_id'=>$row->d_id]]);
                                            if(count($sales_detail) > 0){
                                                $sales_detail = count($sales_detail);
                                            }else{
                                                $sales_detail = 0;
                                            }
                                            $sales = $sales - $sales_detail;
                                            ?>
                                            <?php if ($sales > 0): $iCount+=$sales; ?>

                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; echo $disT=$iCount;?>

                                </h3>

                                <p>Discrepancy Report</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-gears"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/discrepancy/report" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->

                </div>
                <!-- /.row -->
            <?php } ?>

            <div class="row">
                <?php if ($_SESSION['admin']['type'] == '0') { ?>
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><b>MTD</b></h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <div class="chart" id="sales-chart"
                                     style="height:height: 370px;; position: relative;"></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title"><b>Today's</b></h3>

                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                </div>
                            </div>
                            <div class="box-body chart-responsive">
                                <div class="chart" id="sales-chart1"
                                     style="height:height: 370px;; position: relative;"></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <?php if(false){ ?><div class="col-md-6">
                        <div class="box box-info">
                            <div class="box-header">
                                <i class="fa fa-envelope"></i>

                                <h3 class="box-title">Quick Email</h3>
                                <!-- tools box -->
                                <div class="pull-right box-tools">
                                    <button type="button" class="btn btn-info btn-sm" data-widget="remove"
                                            data-toggle="tooltip"
                                            title="Remove">
                                        <i class="fa fa-times"></i></button>
                                </div>
                                <!-- /. tools -->
                            </div>
                            <div class="box-body">
                                <form
                                        enctype="multipart/form-data"
                                        id="form_validate" method="POST"
                                        action="<?= $base_url ?>controllers/admin/mailbox?action=add&dash=1&type=<?php echo $_SESSION['admin']['type'];?>"

                                >
                                    <div class="form-group">
                                        <select name="m_id[]" id="m_id" class="form-control select2" multiple="multiple" data-placeholder="Select Emails"
                                                style="width: 100%;">
                                            <?php
                                            $results = User::all(['order' => 'user_id DESC']);
                                            if (count($results) > 0) {
                                                foreach ($results as $row) {
                                                    ?>
                                                    <option value="<?= $row->user_id?>"><?= $row->user_email;?>
                                                        <?php if($row->user_desg==0){
                                                            echo "Admin";
                                                        }else if($row->user_desg==1){
                                                            echo "User";
                                                        }
                                                        ?>
                                                    </option>
                                                <?php } }?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" name="m_sub" placeholder="Subject:" />
                                    </div>
                                    <div>
                                        <textarea name="m_msg" class="textarea" placeholder="Message"
                                                  style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>

                            </div>
                            <div class="box-footer clearfix">
                                <button type="submit" class="pull-right btn btn-default" id="sendEmail">Send
                                    <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                            </form>
                        </div>
                        </div>
                    <?php }} ?>

            </div>

            <?php if ($_SESSION['admin']['type'] == '1') { ?>
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#B80060; color:white;">
                            <div class="inner">
                                <h3>
                                    <?php
                                    $activitySales=0;
                                    $activity=Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id'])]);
                                    if(count($activity)>0){
                                        foreach($activity as $s){
                                            $activityMonth=date("m", strtotime($s->d_entry_date));
                                            if($activityMonth==date('m')){
                                                $activitySales++;
                                            }
                                        }
                                        echo $activitySales;
                                    }else echo "0";
                                    ?>
                                </h3>

                                <p>Total Activities</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/activities/index" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#474747; color:white;">
                            <div class="inner">
                                <h3>Add</h3>

                                <p>Activities</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-plus"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/activities/add" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#E3027F; color:white;">
                            <div class="inner">
                                <h3>
                                    <?php
                                    $countSales=0;
                                    $sale=SaleDetail::all(['conditions'=>array('s_user_id'=>$_SESSION['admin']['id'])]);
                                    if(count($sale)>0){
                                        foreach($sale as $s){
                                            $saleMonth=date("m", strtotime($s->s_d_entry_date));
                                            if($saleMonth==date('m')){
                                                $countSales++;
                                            }
                                        }
                                        echo $countSales;
                                    }else echo "0";
                                    ?>
                                </h3>

                                <p>Total Sales</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-shopping-cart"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/sales/add" class="small-box-footer">More info <i
                                        class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-xs-6">
                        <!-- small box -->
                        <div class="small-box" style="background:#676767; color:white;">
                            <div class="inner">
                                <h3>View</h3>

                                <p>Settings</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-gears"></i>
                            </div>
                            <a href="<?= $base_url ?>admin/settings/index" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
            <?php } ?>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn btn-danger pull-right" id="export_pdf"><i class="fa fa-file-pdf-o"></i></button>
                    <button type="button" class="btn btn-success pull-right" id="export_excel"><i class="fa fa-file-excel-o"></i></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header text-center">
                            <h3 class="box-title"><b class="label label-primary">CPS</b></h3>
                        </div>

                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example1" class="example1 table table-bordered table-striped">
                                <thead>
                                <tr style="background:#B80060; color:white;">
                                    <th>Rank</th>
                                    <th>Name</th>
                                    <th>Cold Call</th>
                                    <th>Telesales</th>
                                    <th>Referral</th>
                                    <th>Grand Total</th>
                                    <th>Sales Per Day</th>
                                    <th>MPL</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $list = User::find_by_sql('select * from user where user_desg=1 and user_type=1');
                                $cUsers=count($list);
                                $rank=1;
                                $month=date('m');
                                $year=date('Y');
                                $coldCallSumAll=0;
                                $teleCallSumAll=0;
                                $refCallSumAll=0;
                                $grandTotalAll=0;
                                $totalSalePerDayAll=0;
                                $mplTotal=0;
                                foreach($list as $item){
                                    $numofActivities=count(Activities::find_by_sql('select * from d where d_user_id="'.$item->user_id.'" and month(d_entry_date)="'.$month.'" and year(d_entry_date)="'.$year.'"'));
                                    ?>

                                    <tr>
                                        <td><?= $rank;?></td>
                                        <td><?= $item->user_name;?></td>
                                        <td>
                                            <?php
                                            $coldCriteria=Cps::find_by_sql('select * from cps_mpl order by cps_mpl_id DESC LIMIT 1');
                                            $coldcall = SaleDetail::find_by_sql('select * from sale_detail where s_user_id="'.$item->user_id.'" and s_service=1 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo 	$coldCallSum=number_format(count($coldcall),1,'.','');
                                            $coldCallSumAll+=$coldCallSum;
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            $teleSale=Cps::find_by_sql('select * from cps_mpl  order by cps_mpl_id DESC LIMIT 1');
                                            $telecall = SaleDetail::find_by_sql('select * from sale_detail where s_user_id="'.$item->user_id.'" and s_service=2 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo $teleCallSum=number_format( count($telecall),1,'.','');
                                            $teleCallSumAll+=$teleCallSum;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $refSale=Cps::find_by_sql('select * from cps_mpl  order by cps_mpl_id DESC LIMIT 1');
                                            $refcall = SaleDetail::find_by_sql('select * from sale_detail where s_user_id="'.$item->user_id.'" and s_service=3 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo $refCallSum=number_format( count($refcall),1,'.','');
                                            $refCallSumAll+=$refCallSum;
                                            ?>
                                        </td>
                                        <td><?php echo $grandTotal=number_format($coldCallSum+$teleCallSum+$refCallSum,1,'.','');$grandTotalAll+=$grandTotal;?></td>
                                        <td><?php if($numofActivities!=0){echo $totalSalePerDay=number_format($grandTotal/$numofActivities,2,'.','');$totalSalePerDayAll+=$totalSalePerDay;}else echo "0.0";  ?></td>
                                        <?php
                                        if($numofActivities!=0){
                                            $mpl=number_format(($coldCallSum*$coldCriteria[0]->cps_mpl_cold_call)+($teleCallSum*$teleSale[0]->cps_mpl_tele)+($refCallSum*$refSale[0]->cps_mpl_reff),1,'.','')/$numofActivities;
                                            $mplTotal+=$mpl;
                                        }else $mpl=0.0;
                                        $mplCri=Cps::find_by_sql('select * from cps_mpl  order by cps_mpl_id DESC LIMIT 1');
                                        ?>

                                        <td style="background:<?php if($mpl>=$mplCri[0]->cps_mpl_criteria)echo "green;";else echo "red;" ?>border:solid 1px black;color:white;"><?= number_format($mpl,2,'.',''); ?></td>
                                    </tr>
                                    <?php
                                    $rank++;}
                                ?>									</tbody>
                                <tfoot>
                                <tr style="background:#B80060; color:white;">
                                    <th></th>
                                    <th>Grand Total</th>
                                    <th><?= number_format($coldCallSumAll,'1','.',''); ?></th>
                                    <th><?= number_format($teleCallSumAll,'1','.',''); ?></th>
                                    <th><?= number_format($refCallSumAll,'1','.',''); ?></th>
                                    <th><?= number_format($grandTotalAll,'1','.','');?></th>
                                    <th><?php
                                        $cUsersTotalSalesPerDay=number_format($totalSalePerDayAll,'2','.','');
                                        if($cUsers>0)
                                            echo number_format($totalSalePerDayAll/$cUsers,'2','.','');
                                        else
                                            echo "0.0";

                                        ?></th>
                                    <th><?php
                                        if($cUsers>0)
                                            echo number_format($mplTotal/$cUsers,'2','.','');
                                        else
                                            echo "0.0";
                                        ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right" id="export_pdf1"><i class="fa fa-file-pdf-o"></i></button>
                            <button type="button" class="btn btn-success pull-right" id="export_excel1"><i class="fa fa-file-excel-o"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header text-center">
                            <h3 class="box-title"><b class="label label-danger">CC CPS</b></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example2" class="example2 table table-bordered table-striped">
                                <thead>
                                <tr style="background:#B80060; color:white;">
                                    <th>Rank</th>
                                    <th>Name</th>
                                    <th>Cold Call</th>
                                    <th>Telesales</th>
                                    <th>Referral</th>
                                    <th>Grand Total</th>
                                    <th>Sales Per Day</th>
                                    <th>MPL</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $list = User::find_by_sql('select * from user where user_desg=1 and user_type=2');
                                $ccUsers=count($list);
                                $rank=1;
                                $month=date('m');
                                $year=date('Y');
                                $coldCallSumAll=0;
                                $teleCallSumAll=0;
                                $refCallSumAll=0;
                                $grandTotalAll=0;
                                $totalSalePerDayAll=0;
                                $mplTotal=0;
                                foreach($list as $item){
                                    $numofActivities=count(Activities::find_by_sql('select * from d where d_user_id="'.$item->user_id.'" and month(d_entry_date)="'.$month.'" and year(d_entry_date)="'.$year.'"'));
                                    ?>

                                    <tr>
                                        <td><?= $rank;?></td>
                                        <td><?= $item->user_name;?></td>
                                        <td>
                                            <?php
                                            $coldCriteria=Ccps::find_by_sql('select * from ccps_mpl order by ccps_mpl_id DESC LIMIT 1');
                                            $coldcall = SaleDetail::find_by_sql('select * from sale_detail where s_user_id="'.$item->user_id.'" and s_service=1 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo 	$coldCallSum=number_format(count($coldcall),1,'.','');
                                            $coldCallSumAll+=$coldCallSum;
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            $teleSale=Ccps::find_by_sql('select * from ccps_mpl  order by ccps_mpl_id DESC LIMIT 1');
                                            $telecall = SaleDetail::find_by_sql('select * from sale_detail where s_user_id="'.$item->user_id.'" and s_service=2 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo $teleCallSum=number_format( count($telecall),1,'.','');
                                            $teleCallSumAll+=$teleCallSum;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $refSale=Ccps::find_by_sql('select * from ccps_mpl  order by ccps_mpl_id DESC LIMIT 1');
                                            $refcall = SaleDetail::find_by_sql('select * from sale_detail where s_user_id="'.$item->user_id.'" and s_service=3 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo $refCallSum=number_format( count($refcall),1,'.','');
                                            $refCallSumAll+=$refCallSum;
                                            ?>
                                        </td>
                                        <td><?php echo $grandTotal=number_format($coldCallSum+$teleCallSum+$refCallSum,1,'.','');$grandTotalAll+=$grandTotal;?></td>
                                        <td><?php if($numofActivities!=0){echo $totalSalePerDay=number_format($grandTotal/$numofActivities,2,'.','');$totalSalePerDayAll+=$totalSalePerDay;}else echo "0.0";  ?></td>
                                        <?php
                                        if($numofActivities!=0){
                                            $mpl=number_format(($coldCallSum*$coldCriteria[0]->ccps_mpl_cold_call)+($teleCallSum*$teleSale[0]->ccps_mpl_tele)+($refCallSum*$refSale[0]->ccps_mpl_reff),1,'.','')/$numofActivities;
                                            $mplTotal+=$mpl;
                                        }else $mpl=0.0;
                                        $mplCri=Ccps::find_by_sql('select * from ccps_mpl  order by ccps_mpl_id DESC LIMIT 1');
                                        ?>

                                        <td style="background:<?php if($mpl>=$mplCri[0]->ccps_mpl_criteria)echo "green;";else echo "red;" ?>border:solid 1px black;color:white;"><?= number_format($mpl,2,'.',''); ?></td>
                                    </tr>
                                    <?php
                                    $rank++;}
                                ?>									</tbody>
                                <tfoot>
                                <tr style="background:#B80060; color:white;">
                                    <th></th>
                                    <th>Grand Total</th>
                                    <th><?= number_format($coldCallSumAll,'1','.',''); ?></th>
                                    <th><?= number_format($teleCallSumAll,'1','.',''); ?></th>
                                    <th><?= number_format($refCallSumAll,'1','.',''); ?></th>
                                    <th><?= number_format($grandTotalAll,'1','.','');?></th>
                                    <th><?php
                                        $ccUsersSalesPerDay=number_format($totalSalePerDayAll,'2','.','');
                                        if($ccUsers>0)
                                            echo number_format($totalSalePerDayAll/$ccUsers,'2','.','');
                                        else
                                            echo "0.0";

                                        ?></th>
                                    <th><?php
                                        if($ccUsers>0)
                                            echo number_format($mplTotal/$ccUsers,'2','.','');
                                        else
                                            echo "0.0";
                                        ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


                <!-- Team Total Start-->
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right" id="export_pdf2"><i class="fa fa-file-pdf-o"></i></button>
                            <button type="button" class="btn btn-success pull-right" id="export_excel2"><i class="fa fa-file-excel-o"></i></button>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header text-center">
                            <h3 class="box-title"><b class="label label-warning">Team Total</b></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example3" class="example3 table table-bordered table-striped">
                                <thead>
                                <tr style="background:#B80060; color:white;">
                                    <th>Cold Call</th>
                                    <th>Telesales</th>
                                    <th>Referral</th>
                                    <th>Grand Total</th>
                                    <th>Sales Per Day</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $list = User::find_by_sql('select * from user where user_desg=1 and user_status=1');
                                $allUsers=count($list);
                                $rank=1;
                                $month=date('m');
                                $year=date('Y');
                                foreach($list as $item){
                                    $numofActivities=count(Activities::find_by_sql('select DISTINCT d_entry_date from d where month(d_entry_date)="'.$month.'" and year(d_entry_date)="'.$year.'"'));
                                    ?>

                                    <tr style="display:none;">
                                        <td>
                                            <?php
                                            $coldcall = SaleDetail::find_by_sql('select * from sale_detail where s_service=1 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo 	$coldCallSum=number_format(count($coldcall),1,'.','');
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            $telecall = SaleDetail::find_by_sql('select * from sale_detail where s_service=2 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo $teleCallSum=number_format( count($telecall),1,'.','');
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $refcall = SaleDetail::find_by_sql('select * from sale_detail where s_service=3 and month(s_d_entry_date)="'.$month.'" and year(s_d_entry_date)="'.$year.'"');
                                            echo $refCallSum=number_format( count($refcall),1,'.','');
                                            ?>
                                        </td>
                                        <td><?php echo $grandTotal=number_format($coldCallSum+$teleCallSum+$refCallSum,1,'.','');?></td>
                                        <td><?php if($numofActivities!=0){echo $totalSalePerDay=number_format($grandTotal/$numofActivities,1,'.','');}else echo "0.0";  ?></td>

                                    </tr>
                                    <?php
                                    $rank++;}


                                ?>


                                <tr>
                                    <td><?= @$coldCallSum; ?></td>
                                    <td><?= @$teleCallSum; ?></td>
                                    <td><?= @$refCallSum; ?></td>
                                    <td><?= @$grandTotal; ?></td>
                                    <td><?php if($allUsers>0)echo number_format(($cUsersTotalSalesPerDay+$ccUsersSalesPerDay)/$allUsers,2,'.',''); else echo "0.0";  ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


                <!-- Team Total End-->


				
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger pull-right" id="export_pdf3"><i class="fa fa-file-pdf-o"></i></button>
                            <button type="button" class="btn btn-success pull-right" id="export_excel3"><i class="fa fa-file-excel-o"></i></button>
                        </div>
                    </div>
                </div>				
				
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header text-center">
                            <h3 class="box-title"><b class="label label-success">SPD</b></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example4" class="example4 table table-bordered table-striped">
                                <thead>
                                <tr style="background:#B80060; color:white;">
                                    <th>Total Appt.</th>
                                    <th>Total Sales</th>
                                    <th>Self Gen</th>
                                    <th>Conversion</th>
                                    <th>SPD</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $date=date('Y-m-d');
                                $totalNumOfActivities=count(Activities::find_by_sql('select *  from d where d_entry_date="'.$date.'"'));
                                $totalAppts=Activities::find_by_sql('select SUM(d_appt_set_diary) as appt_in_diary from d where d_entry_date="'.$date.'"');
                                    ?>

                                    <tr style="display:none;">
                                        <td>
                                            <?php
                                            $coldcallSPD = SaleDetail::find_by_sql('select * from sale_detail where s_service=1 and s_d_entry_date="'.$date.'"');
                                            echo 	$coldCallSumSPD=number_format(count($coldcallSPD),2,'.','');
                                            ?>

                                        </td>
                                        <td>
                                            <?php
                                            $telecallSPD = SaleDetail::find_by_sql('select * from sale_detail where s_service=2 and s_d_entry_date="'.$date.'"');
                                            echo $teleCallSumSPD=number_format( count($telecallSPD),2,'.','');
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $refcallSPD = SaleDetail::find_by_sql('select * from sale_detail where s_service=3 and s_d_entry_date="'.$date.'"');
                                            echo $refCallSumSPD=number_format( count($refcallSPD),2,'.','');
                                            ?>
                                        </td>
                                        <td><?php echo $grandTotalSPD=number_format($coldCallSumSPD+$teleCallSumSPD+$refCallSumSPD,2,'.','');?></td>
                                    </tr>

                                <tr>
                                    <td><?= @$totalAppts=number_format($totalAppts[0]->appt_in_diary,2,'.',''); ?></td>
                                    <td><?= @$grandTotalSPD; ?></td>
                                    <td><?= @number_format($coldCallSumSPD+$refCallSumSPD,2,'.',''); ?></td>
                                    <td><?= @number_format(($grandTotalSPD*100)/$totalAppts,2,'.',''); ?></td>
                                    <td><?= @number_format($grandTotalSPD/$totalNumOfActivities,2,'.','');?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
				
				
				
				

            </div>













        </section>



    </div>





    <?php

    $total = count(User::find('all', ['conditions' => ['user_desg' => 1]]));
    $active = count(User::find('all', ['conditions' => ['user_status' => 1, 'user_desg' => 1]]));
    $non_active = count(User::find('all', ['conditions' => ['user_status' => 0, 'user_desg' => 1]]));

    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script src="<?= $base_url; ?>assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?= $base_url; ?>assets/admin/table_export/tableExport.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/table_export/libs/js-xlsx/xlsx.core.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/table_export/libs/jsPDF/jspdf.min.js"></script>
<script src="<?= $base_url; ?>assets/admin/table_export/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2()
        var table=$('#example1').DataTable({
            "pageLength": 25,
            "order": [[ 7, "desc" ]]
        });
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        var t=$('#example2').DataTable({
            "pageLength": 25,
            "order": [[ 7, "desc" ]]
        });
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    });
</script>
<script>
    var tableToExcel = (function () {
        var uri = 'data:application/vnd.ms-excel;base64,',
            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
            , base64 = function (s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            }
            , format = function (s, c) {
                return s.replace(/{(\w+)}/g, function (m, p) {
                    return c[p];
                })
            };
        return function (table, name) {
            if (!table.nodeType) table = document.querySelector(table);
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
            window.location.href = uri + base64(format(template, ctx));
        }
    })();
    $('body').on('click','#export_excel',function(){
        tableToExcel('.example1','date-to-date-report');
    });
    $('body').on('click','#export_excel1',function(){
        tableToExcel('.example2','date-to-date-report');
    });
    $('body').on('click','#export_excel2',function(){
        tableToExcel('.example3','date-to-date-report');
    });
    $('body').on('click','#export_excel3',function(){
        tableToExcel('.example4','date-to-date-report');
    });	




    $('body').on('click','#export_pdf',function(){
        $('.example1').tableExport({
            fileName: 'cps',
            type:'pdf',
            bootstrap: true,
            jspdf: {orientation: 'l',
                format: 'a4',
                margins: {left:10, right:10, top:20, bottom:20},
                autotable: {
                    tableWidth: 'auto'}
            }
        });
    });
    $('body').on('click','#export_pdf1',function(){
        $('.example2').tableExport({
            fileName: 'cc-cps',
            type:'pdf',
            bootstrap: true,
            jspdf: {orientation: 'l',
                format: 'a4',
                margins: {left:10, right:10, top:20, bottom:20},
                autotable: {
                    tableWidth: 'auto'}
            }
        });
    });
    $('body').on('click','#export_pdf2',function(){
        $('.example3').tableExport({
            fileName: 'team-total',
            type:'pdf',
            bootstrap: true,
            jspdf: {orientation: 'l',
                format: 'a4',
                margins: {left:10, right:10, top:20, bottom:20},
                autotable: {
                    tableWidth: 'auto'}
            }
        });
    });
	
    $('body').on('click','#export_pdf3',function(){
        $('.example4').tableExport({
            fileName: 'SPD',
            type:'pdf',
            bootstrap: true,
            jspdf: {orientation: 'l',
                format: 'a4',
                margins: {left:10, right:10, top:20, bottom:20},
                autotable: {
                    tableWidth: 'auto'}
            }
        });
    });	

</script>

