<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Discrepancy Report';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
   		require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    	require_once $app_path . 'views/admin/includes/sidebar.php';
     
    if($_SESSION['admin']['type']=='0'){	
	$results = Activities::find_by_sql('SELECT * , CAST(d_entry_date AS DATE) AS d_entry_date FROM d JOIN user ON user.user_id =  d.d_user_id ORDER BY d_entry_date DESC');
	}else{
	$results = Activities::find_by_sql('SELECT * , CAST(d_entry_date AS DATE) AS d_entry_date FROM d JOIN user ON user.user_id =  d.d_user_id and user.user_id='.$_SESSION["admin"]["id"].' ORDER BY d_entry_date DESC');
	}
	    
    ?>
	

	
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Discrepancy Report
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
							<div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
							<div class="clear10"></div>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="10%">Sr#</th>
									<th width="25%">User Name</th>
									<th width="25%">Activity Date</th>
									<th width="30%">Sales Remaining</th>
                                </tr>
                                </thead>
                                <tbody>
                                	<?php if (count($results) > 0): ?>
                                		<?php foreach ($results as $key => $row): ?>
                                			<?php 
                                				$sales = $row->d_sales;
                                				$sales_detail = SaleDetail::find('all',['conditions'=>['s_daily_id'=>$row->d_id]]);
                                				if(count($sales_detail) > 0){
                                					$sales_detail = count($sales_detail);
                                				}else{
                                					$sales_detail = 0;
                                				}
                                				$sales = $sales - $sales_detail;
                                			?>
                                			<?php if ($sales > 0): ?>
			                                	<tr>
				                                    <td width="10%"><?= ($key+1) ?></td>
													<td width="25%"><?= $row->user_name; ?></td>
													<td width="25%"><?= date('d-m-Y',strtotime($row->d_entry_date)); ?></td>
													<td width="30%"><?= $sales; ?></td>
			                                	</tr>
                                			<?php endif ?>
                                		<?php endforeach ?>
                                	<?php endif ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>