<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Update MPL Calculations For CPS';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
<style>
.error{
	color:#A94442;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
	
	if(isset($_GET['cps_mpl_id'])){
		$id=$_GET['cps_mpl_id'];
		$result = Cps::find(['conditions' => ['cps_mpl_id' => $id]]);
	}
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Update MPL Calculations For CPS
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <div class="col-xs-12"><?php require_once $app_path . 'views/errors.php'; ?></div>
                         <div class="clear20"></div>						
                        <form role="form"
                              class="col-md-12"
                              enctype="multipart/form-data"
                              id="form_validate" method="POST"
                              action="<?= $base_url ?>controllers/admin/leaderboard?action=update&cps=1&cps_mpl_id=<?= $id;?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="cps_formula">MPL Criteria For CPS</label>
                                    <input required type="text" class="form-control" name="cps_formula"
                                              id="cps_formula"
                                              placeholder="MPL Criteria Value CPS" value="<?= $result->cps_mpl_criteria; ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="cps_coldcall">Cold Call Criteria For CPS</label>
                                    <input required type="text" class="form-control" name="cps_coldcall"
                                              id="cps_coldcall"
                                              placeholder="Cold Call Criteria Value CPS" value="<?= $result->cps_mpl_cold_call; ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="cps_ref">Referral Criteria For CPS</label>
                                    <input required type="text" class="form-control" name="cps_ref"
                                              id="cps_ref"
                                              placeholder="Referral Criteria Value CPS" value="<?= $result->cps_mpl_reff; ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="cpd_tele">TeleSale Criteria For CPS</label>
                                    <input required type="text" class="form-control" name="cps_tele"
                                              id="cpd_tele"
                                              placeholder="TeleSale Criteria Value CPS" value="<?= $result->cps_mpl_tele; ?>" />
                                </div>									
								<div class="form-group">
                                    <label for="cpsdate">Month (Select Any Date)</label>
									<input id="cpsdate" placeholder="Select Any Date" type="text" class="form-control datepicker" value="<?= date('d-m-Y',strtotime($result->cps_mpl_date));?>" name="cpsdate">
                                </div>								
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
        $('.datepicker').datepicker({
            autoclose:true,
			format:'dd-mm-yyyy'
        });
		$("#form_validate1").validate();
</script>