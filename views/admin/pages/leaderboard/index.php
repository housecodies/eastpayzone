<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Add MPL Calculations';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
<style>
.error{
	color:#A94442;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add MPL Calculations
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <div class="col-xs-12"><?php require_once $app_path . 'views/errors.php'; ?></div>
                         <div class="clear20"></div>						
                        <form role="form"
                              class="col-md-6"
                              enctype="multipart/form-data"
                              id="form_validate" method="POST"
                              action="<?= $base_url ?>controllers/admin/leaderboard?action=add&cps=1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="cps_formula">MPL Criteria For CPS</label>
                                    <input required type="text" class="form-control" name="cps_formula"
                                              id="cps_formula"
                                              placeholder="MPL Criteria Value CPS" />
                                </div>
                                <div class="form-group">
                                    <label for="cps_coldcall">Cold Call Value For CPS</label>
                                    <input required type="text" class="form-control" name="cps_coldcall"
                                              id="cps_coldcall"
                                              placeholder="Cold Call Value CPS" />
                                </div>
                                <div class="form-group">
                                    <label for="cps_ref">Referral Value For CPS</label>
                                    <input required type="text" class="form-control" name="cps_ref"
                                              id="cps_ref"
                                              placeholder="Referral Value CPS" />
                                </div>
                                <div class="form-group">
                                    <label for="cpd_tele">TeleSale Value For CPS</label>
                                    <input required type="text" class="form-control" name="cps_tele"
                                              id="cpd_tele"
                                              placeholder="TeleSale Value CPS" />
                                </div>									
								<div class="form-group">
                                    <label for="cpsdate">Month (Select Any Date)</label>
									<input id="cpsdate" placeholder="Select Any Date" type="text" class="form-control datepicker" value="" name="cpsdate">
                                </div>								
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
						<form role="form"
                              class="col-md-6"
                              enctype="multipart/form-data"
                              id="form_validate1" method="POST"
                              action="<?= $base_url ?>controllers/admin/leaderboard?action=add&ccps=1">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="ccps_formula">MPL Criteria For CC CPS</label>
                                    <input required type="text" class="form-control" name="ccps_formula"
                                              id="ccps_formula"
                                              placeholder="MPL Criteria Value CC CPS" />
                                </div>
                                <div class="form-group">
                                    <label for="ccps_coldcall">Cold Call Value For CC CPS</label>
                                    <input required type="text" class="form-control" name="ccps_coldcall"
                                              id="ccps_coldcall"
                                              placeholder="Cold Call Value CC CPS" />
                                </div>
                                <div class="form-group">
                                    <label for="ccps_ref">Referral Value For CC CPS</label>
                                    <input required type="text" class="form-control" name="ccps_ref"
                                              id="ccps_ref"
                                              placeholder="Referral Value CC CPS" />
                                </div>
                                <div class="form-group">
                                    <label for="ccps_tele">TeleSale Value For CC CPS</label>
                                    <input required type="text" class="form-control" name="ccps_tele"
                                              id="ccps_tele"
                                              placeholder="TeleSale Value CC CPS" />
                                </div>								
								<div class="form-group">
                                    <label for="ccpsdate">Month (Select Any Date)</label>
									<input id="ccpsdate" placeholder="Select Any Date" type="text" class="form-control datepicker" value="" name="ccpsdate">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
        $('.datepicker').datepicker({
            autoclose:true,
			format:'dd-mm-yyyy'
        });
		$("#form_validate1").validate();
</script>
