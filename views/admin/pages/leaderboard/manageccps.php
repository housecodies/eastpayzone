<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Manage MPL Calculations For CC CPS';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Manage MPL Calculations For CC CPS
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">Sr#</th>
                                    <th width="20%">MPL Criteria</th>
                                    <th width="10%">Cold Call Criteria</th>
                                    <th width="15%">Referral Criteria</th>
									<th width="20%">TeleSale Criteria</th>
									<th width="20%">Month (Date)</th>
                                    <!--<th width="20%">Action</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $results = Ccps::all(['order' => 'ccps_mpl_id DESC']);
                                if (count($results) > 0) {
                                    $index = 1;
                                    foreach ($results as $row) { ?>
                                        <tr style="<?php if($index==1)echo 'background:green !important;color:white';?>">
                                            <td width="5%"><?= $index++; ?></td>
                                            <td width="20%"><?= $row->ccps_mpl_criteria; ?></td>
                                            <td width="10%"><?= $row->ccps_mpl_cold_call; ?></td>
                                            <td width="15%"><?= $row->ccps_mpl_reff; ?></td>											
                                            <td width="15%"><?= $row->ccps_mpl_tele; ?></td>											
                                            <td width="15%"><?= date('d-m-Y',strtotime($row->ccps_mpl_date)); ?></td>											                                         
                                            <!--<td width="20%">
                                                <button class="btn btn-primary btn-xs" type="button"
                                                        onclick="window.location.href='<?= $base_url . 'controllers/admin/leaderboard?ccps=1&action=edit&ccps_mpl_id=' . encode_url($row->ccps_mpl_id) ?>'">
                                                    <i class="fa fa-pencil"></i></button>
                                                <button class="btn btn-danger btn-xs" type="button"
                                                        onclick="delete_object('<?= $base_url . 'controllers/admin/leaderboard?ccps=1&action=delete&ccps_mpl_id=' . encode_url($row->ccps_mpl_id) ?>')">
                                                    <i class="fa fa-trash"></i></button>
                                            </td>-->
                                        </tr>
                                    <?php }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
