<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Manage Sales';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
<link rel="stylesheet" href="<?= $base_url; ?>assets/admin/bootstrap-daterangepicker/daterangepicker.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
	if(@$_GET['submit'] && $_GET['submit']=="Search"){
		if(@$_GET['userdate'] && @$_GET['username']){
			$user_ids=implode("','",$_GET['username']);
			$user_date=$_GET['userdate'];
        	$date_range = explode(' - ',$user_date);
        	$start_date = $date_range[0];
			$start_date = str_replace('/', '-', $start_date);
			$start_date = date('Y-m-d', strtotime($start_date));

			$end_date = $date_range[1];
			$end_date = str_replace('/', '-', $end_date);
			$end_date = date('Y-m-d', strtotime($end_date));
			$results = SaleDetail::find_by_sql("SELECT * FROM sale_detail WHERE s_user_id IN ('".$user_ids."') AND s_d_entry_date >= '".$start_date."' AND s_d_entry_date <= '".$end_date."' ORDER BY s_d_entry_date DESC");		
		}else if(@$_GET['username']){
			$user_ids=implode("','",$_GET['username']);
			$results = SaleDetail::find_by_sql("SELECT * FROM sale_detail WHERE s_user_id IN ('".$user_ids."') ORDER BY s_d_entry_date DESC");
		}else if(@$_GET['userdate']){
			$user_date=$_GET['userdate'];
        	$date_range = explode(' - ',$user_date);
        	$start_date = $date_range[0];
			$start_date = str_replace('/', '-', $start_date);
			$start_date = date('Y-m-d', strtotime($start_date));

			$end_date = $date_range[1];
			$end_date = str_replace('/', '-', $end_date);
			$end_date = date('Y-m-d', strtotime($end_date));
			$results = SaleDetail::find_by_sql("SELECT * FROM sale_detail WHERE s_d_entry_date >= '".$start_date."' AND s_d_entry_date <= '".$end_date."' ORDER BY s_d_entry_date DESC");	
		}else{
			$results = SaleDetail::all(['order' => 's_d_entry_date DESC','limit' => 240]);
		}

	}else {
			$results = SaleDetail::all(['order' => 's_d_entry_date DESC','limit' => 240]);
		}
	
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Manage Sales
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
							<div class="row">
							  <form action="<?= $base_url;?>admin/sale-reports/index" method="GET">
								   <div class="col-sm-6">
                                    <label>User Name</label>
                                    <select name="username[]"  class="form-control select2" multiple="multiple" data-placeholder="Select Users">
									   <?php  
									   
													$results1 = User::all(['conditions'=>array('user_desg'=>1),'order' => 'user_id DESC']);
																if (count($results1) > 0) {
																	$l=0;
																	foreach ($results1 as $row1) {
									   ?>				
												  <option 
												  <?php if(@$_GET['username'] && in_array($row1->user_id, $_GET['username']) ){?>
												     selected="selected"
												  <?php }?>

												  value="<?= $row1->user_id?>"><?= $row1->user_email?></option>
									   <?php $l++;}  }?> 	
                                    </select>								   
								   </div>
								   <div class="col-sm-6">
								   <label>Date</label>	
								   <input  type="text" class="form-control" id="date_range" value="<?php echo (@$_GET['userdate'])? $_GET['userdate'] :'';?>" name="userdate" />
								   </div>
								   <div class="col-xs-12">
								   <br>
								   <input type="submit" name="submit" value="Search" class="btn btn-default" style="text-align: center;margin: 0 auto;display: block;"/>
                       			   </div>				
						   	 </form>
							</div>
							<div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
							<div class="clear10"></div>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">Sr#</th>
									<th width="15%">User Name</th>
									<th width="15%">Trading Name</th>
                                    <th width="15%">Lead Source</th>
                                    <th width="5%">Package ID</th>
									<th width="5%">Activity Date</th>
									<th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
							
                                
								if (count($results) > 0) {
                                    $index = 1;
                                    foreach ($results as $row) {
									?>
                                        <tr>
                                            <td width="5%"> <?= $row->s_id; ?></td>
											<td width="15%"><?php 
											$username=User::find_by_sql('SELECT * FROM user WHERE user_id = "'.$row->s_user_id.'" ORDER BY user_id DESC'); 
                                             echo $username[0]->user_name; 
											?></td>
											<td width="15%"><?= $row->s_customer_name; ?></td>
                                            <td width="15%">
											<?php 
											if($row->s_service==1){
											  echo "Cold Call";
											}else if($row->s_service==2){
												echo "TeleSales";
											}else{
												echo "Referral Sale";
											}												
											?>
											</td>
                                            <td width="10%"><?= $row->s_package; ?></td>
											<td width="10%"><?= date('d-m-Y',strtotime($row->s_d_entry_date)); ?></td>
											<td width="15%">
											<button class="btn btn-primary btn-xs" type="button" onclick="window.location.href='<?= $base_url;?>controllers/user/sales?action=edit&s_id=<?= $row->s_id; ?>'">
                                                    <i class="fa fa-pencil"></i></button>
										    <button class="btn btn-danger btn-xs" type="button"
                                                        onclick="delete_object('<?= $base_url . 'controllers/user/sales?action=delete&s_id=' .$row->s_id?>')">
                                                    <i class="fa fa-trash"></i></button>		
											
											</td>
                                        </tr>
                                    <?php
                                }}
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
	<script src="<?= $base_url; ?>assets/admin/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
    	$('#date_range').daterangepicker(
	      {
	      	maxDate: '<?= date("d/m/Y"); ?>',
	        timePicker: false, 
	        locale: {
		      format: 'DD/MM/YYYY'
		    }
	      },
	    );
	    <?php if(!@$_GET['userdate']){ ?>
	    	$('#date_range').val('');
		<?php } ?>
	</script>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
