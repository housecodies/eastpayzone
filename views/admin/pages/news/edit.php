<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Edit News';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

if (!isset($_GET['news_id']) || $_GET['news_id'] == "") {
    $msg['errors'] = "There might be some errors. Try again later.";
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/news/index");
} else {
    $id = decode_url($_GET['news_id']);
    $data = News::find(['conditions' => ['news_id' => $id]]);
    if ($data == "") {
        $msg['errors'] = "No Record Found.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/news/index");
    }
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit News
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <form role="form"
                              class="col-md-offset-3 col-md-6 col-md-offset-2 col-md-8 col-md-offset-3 col-md-6"
                              enctype="multipart/form-data"
                              id="form_validate" method="POST"
                              action="<?= $base_url ?>controllers/admin/news?action=update&news_id=<?= encode_url($data->news_id); ?>">
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input required type="text" class="form-control" name="title" id="title"
                                           placeholder="Enter Title" value="<?= $data->news_title; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea required minlength="10" class="form-control textarea" name="description"
                                              id="description"
                                              placeholder="Enter Description"><?= $data->news_description; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input class="form-control" type="file" id="image" name="image">
                                </div>
                                <div class="form-group">
                                    <img class="img-responsive"
                                         src="<?= $base_url . $data->news_image . "small." . $data->news_image_type; ?>"
                                         alt="<?= $data->news_title; ?>">
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
