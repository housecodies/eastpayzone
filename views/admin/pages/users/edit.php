<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Edit User';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

if (!isset($_GET['user_id']) || $_GET['user_id'] == "") {
    $msg['errors'] = "There might be some errors. Try again later.";
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/user/index");
} else {
    $id = decode_url($_GET['user_id']);
    $data = User::find(['conditions' => ['user_id' => $id]]);
    if ($data == "") {
        $msg['errors'] = "No Record Found.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/user/index");
    }
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit User
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <form role="form"
                              class="col-md-offset-3 col-md-6 col-md-offset-2 col-md-8 col-md-offset-3 col-md-6"
                              enctype="multipart/form-data"
                              id="form_validate" method="POST"
                              action="<?= $base_url ?>controllers/admin/user?action=update&user_id=<?= encode_url($data->user_id); ?>">
                            <?php require_once $app_path . 'views/errors.php'; ?>
		                    	<div class="msg"></div>	
                                    <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input required type="text" class="form-control" name="name" id="name"
                                           placeholder="Enter Name" value="<?php echo $data->user_name; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input required type="email" class="form-control" name="email"
                                              id="email"
                                              placeholder="Enter Email"value="<?php echo $data->user_email; ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input  type="password" minlength="6"  class="form-control" name="password"
                                              id="password"
                                              placeholder="Enter Password"/>
                                </div>
								
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text"  class="form-control" name="phone"
                                              id="phone"
                                              placeholder="Enter Phone"value="<?php echo $data->user_phone; ?>" />
                                </div>
                                <div class="form-group">
                                    <label for="designation">Designation</label>
                                    <select required class="form-control" id="designation" name="designation">
										<option <?php if($data->user_desg==0) {echo 'selected="selected"';} ?> value="0">
										Admin
										</option>
										<option <?php if($data->user_desg==1) {echo 'selected="selected"';} ?> value="1">
										User
										</option>
										
									</select>                               
								</div>
								<?php if($data->user_desg==1){?>
								<div class="form-group" id="usertypeContainer">
                                    <label for="usertype">User Type</label>
                                    <select  required class="form-control" id="usertype" name="usertype">
										<option <?php if($data->user_type==1) {echo 'selected="selected"';} ?> value="1">
										CPS
										</option>
										<option <?php if($data->user_type==2) {echo 'selected="selected"';} ?> value="2">
										CC CPS
										</option>
										
									</select>                                
								</div>
                                <?php }?>
                                <?php if($data->user_desg==1){?>
								<div class="form-group" id="usertypeContainer">
                                    <label for="userpror">User Priority (Assign Display Order on Activity Report)</label>
                                    <input type="number" name="user_pror" id="userpror" required min="1" class="form-control" value="<?php if($data->user_pror!=0){echo $data->user_pror;}?>" />                              
								</div>                                 
                                <?php }?>								
                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input  class="form-control" type="file" id="image" name="image">
                                </div>
                            </div>
                     <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<script>

$('body').on('keyup','#email',function(event){
	event.preventDefault();
	let email = $(this).val();
	data = {email:email,user_id:'<?= $data->user_id; ?>'};
	$.ajax({
		url: '<?= $base_url.'controllers/admin/user?action=email_validation'; ?>',
		type: 'POST',
		data: data,
		success: function(data){
			data = data.trim();
			if(data == 'success'){
				$('.box-footer').find('button').removeAttr('disabled');
				$('.msg').html('');
			}else{
				$('.box-footer').find('button').attr('disabled','disabled');
				$('.msg').html('<div class="alert alert-danger alert-dismissable fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong> <i class="fa fa-close"></i> Failed! </strong> Email already exist.</div>');
			}
		},
	});
});
</script>
<?php
unset($_SESSION['admin']['msg']);
?>
