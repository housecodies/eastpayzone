<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'View Email';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
	$messageData=Mailbox::all(['conditions'=>array('m_id'=>$_GET['m_id']),]);
	if(@$_GET['from']){
		$user=User::all(['conditions'=>array('user_id'=>$messageData[0]->m_from_id)]);
	}else if(@$_GET['to']){
		$user=User::all(['conditions'=>array('user_id'=>$messageData[0]->m_to_id)]);
	}
	if(@$_GET['from']){
$m_id=$_GET['m_id'];
$mailbox=Mailbox::find(['conditions'=>array('m_id'=>$m_id)]);
$mailbox->m_status=1;
$mailbox->save();
}
    ?>

    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="<?= $base_url; ?>admin/mailbox/index" class="btn btn-primary btn-block margin-bottom">Back to Inbox</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="<?= $base_url; ?>admin/mailbox/index"><i class="fa fa-inbox"></i> Inbox
                  					<span class="label label-danger pull-right"><?= count(Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id'],'m_status'=>0),'order'=>'m_id DESC']));?></span></a></li>
                <li><a href="<?= $base_url; ?>admin/mailbox/sent"><i class="fa fa-envelope-o"></i> Sent</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Read Mail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding" id="printarea">
              <div class="mailbox-read-info">
                <h3><?= $messageData[0]->m_sub; ?></h3>
                <h5><?php echo $user[0]->user_email; ?>
                  <span class="mailbox-read-time pull-right"><?php echo $messageData[0]->m_created_at; ?></span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-controls with-border text-center">
			  <?php if(@$_GET['from']){?>
                <div class="btn-group">
                  <a href="<?= $base_url; ?>admin/mailbox/add?id=<?= $user[0]->user_id; ?>" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                    <i class="fa fa-reply"></i></a>
                </div>
			  <?php }?>	
                <!-- /.btn-group -->
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                <p>
				<?php echo $messageData[0]->m_msg; ?>
				</p>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    </div>
    
<?php
require_once $app_path . 'views/admin/includes/footer.php';
require_once $app_path . 'views/admin/includes/foot.php';
?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
$('.select2').select2();

</script>