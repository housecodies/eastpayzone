<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Manage Sales';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
         <section class="content-header">
      <h1>
        Mailbox
        <small><?= count(Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id'],'m_status'=>0),'order'=>'m_id DESC']));?> new messages</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mailbox</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="<?= $base_url ?>admin/mailbox/add" class="btn btn-primary btn-block margin-bottom">Compose</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="<?= $base_url; ?>admin/mailbox/index"><i class="fa fa-inbox"></i> Inbox
                  					<span class="label label-danger pull-right"><?= count(Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id'],'m_status'=>0),'order'=>'m_id DESC']));?></span></a></li>
                <li><a href="<?= $base_url; ?>admin/mailbox/sent"><i class="fa fa-envelope-o"></i> Sent</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
                     <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">Sr#</th>
                                    <th width="20%">From</th>
                                    <th width="20%">Subject</th>
                                    <th width="10%">Message</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $results = Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id']),'order' => 'm_id DESC']);
                                if (count($results) > 0) {
                                    $index = 1;
                                    foreach ($results as $row) { ?>
                                        <tr class="<?php if($row->m_status==0) echo 'bg-red disabled color-palette';?>">
                                            <td width="5%"><?= $index++; ?></td>
                                            <td width="20%"><a style="color:darkblue;" href="<?= $base_url.'admin/mailbox/read?m_id='.$row->m_id;?>&from=1"><?php $user=User::all(['conditions'=>array('user_id'=>$row->m_from_id)]); echo $user[0]->user_email; ?></a></td>
                                            <td width="20%"><?= substr($row->m_sub,0,30); ?>...</td>
                                            <td width="10%"><?= substr($row->m_msg,0,45); ?>...</td>
                                        </tr>
                                    <?php }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

        </div>
        </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->    

    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
