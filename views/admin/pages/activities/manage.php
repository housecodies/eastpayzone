<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Manage Activities';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}
require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
	if(@$_GET['submit'] && $_GET['submit']=="Search"){
		if(@$_GET['userdate'] && @$_GET['username']){
			$user_ids=implode("','",$_GET['username']);
			$user_date=$_GET['userdate'];
			$results = Activities::find_by_sql("SELECT * FROM d WHERE d_user_id IN ('".$user_ids."') AND d_entry_date='".date('Y-m-d',strtotime($user_date))."' ORDER BY d_entry_date DESC");		
		}else if(@$_GET['username']){
			
			$user_ids=implode("','",$_GET['username']);
			$results = Activities::find_by_sql("SELECT * FROM d WHERE d_user_id IN ('".$user_ids."') ORDER BY d_entry_date DESC");
			
		}else if(@$_GET['userdate']){
			$user_date=$_GET['userdate'];
			$results = Activities::find_by_sql("SELECT * FROM d WHERE d_entry_date='".date('Y-m-d',strtotime($user_date))."' ORDER BY d_entry_date DESC");
		}else{
			$results = Activities::all(['order' => 'd_entry_date DESC','limit' => 240]);
		}

	}else {
			$results = Activities::all(['order' => 'd_entry_date DESC','limit' => 240]);
		}
	
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Manage Activities
            </h1>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body table-responsive">
                            <div class="clear10"></div>
							<div class="row">
							  <form action="<?= $base_url;?>admin/activities/manage" method="GET">
								   <div class="col-sm-6">
                                    <label>User Name</label>
                                    <select name="username[]"  class="form-control select2" multiple="multiple" data-placeholder="Select Users">
									   <?php  
									   
													$results1 = User::all(['conditions'=>array('user_desg'=>1),'order' => 'user_id DESC']);
																if (count($results1) > 0) {
																	$l=0;
																	foreach ($results1 as $row1) {
									   ?>				
												  <option 
												  <?php if(@$_GET['username'] && in_array($row1->user_id, $_GET['username']) ){?>
												     selected="selected"
												  <?php }?>

												  value="<?= $row1->user_id?>"><?= $row1->user_email?></option>
									   <?php $l++;}  }?> 	
                                    </select>								   
								   </div>
								   <div class="col-sm-6">
								   <label>Date</label>	
								   <input  type="text" class="form-control datepicker" value="<?php echo (@$_GET['userdate'])? $_GET['userdate'] :'';?>" name="userdate" />
								   </div>
								   <div class="col-xs-12">
								   <br>
								   <input type="submit" name="submit" value="Search" class="btn btn-default" style="text-align: center;margin: 0 auto;display: block;"/>
                       			   </div>				
						   	 </form>
							</div>
							<div class="clear10"></div>
                            <?php require_once $app_path . 'views/errors.php'; ?>
							<div class="clear10"></div>
                            <table class="data_tables table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="5%">Sr#</th>
									<th width="15%">User Name</th>
									<th width="15%">Acitivity Date</th>
                                    <th width="15%">Appts In Diary</th>
                                    <th width="5%">Appts Sat</th>
									<th width="5%">Sales</th>
									<th width="15%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
							
                                
								if (count($results) > 0) {
                                    $index = 1;
                                    foreach ($results as $row) {
									?>
                                        <tr>
                                            <td width="5%"> <?= $row->d_id; ?></td>
											<td width="15%"><?php 
											$username=User::find_by_sql('SELECT * FROM user WHERE user_id = "'.$row->d_user_id.'" ORDER BY user_id DESC'); 
                                             echo $username[0]->user_name; 
											?></td>
											<td width="15%"><?= date('d-m-Y',strtotime($row->d_entry_date)); ?></td>
                                            <td width="15%">
											<?php 
											echo $row->d_appt_set_diary;											
											?>
											</td>
                                            <td width="10%"><?= $row->d_appt; ?></td>
											<td width="10%"><?= $row->d_sales; ?></td>
											<td width="15%">
											<button class="btn btn-primary btn-xs" type="button" onclick="window.location.href='<?= $base_url;?>controllers/user/activities?action=edit&d_id=<?= $row->d_id; ?>'">
                                                    <i class="fa fa-pencil"></i></button>
										    <button class="btn btn-danger btn-xs" type="button"
                                                        onclick="delete_object('<?= $base_url . 'controllers/user/activities?action=delete&d_id=' .$row->d_id?>')">
                                                    <i class="fa fa-trash"></i></button>		
											
											</td>
                                        </tr>
                                    <?php
                                }}
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
        $('.datepicker').datepicker({
            autoclose:true,
			format:'dd-mm-yyyy'
        });
</script>