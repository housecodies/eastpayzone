<?php
session_start();
require_once '../../../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

$author = "";
$keywords = "";
$description = "";
$page_name = 'Add Daily Activity';

if (admin_logged_in($_SESSION) == 0) {
    unset($_SESSION['admin']);
    redirect($base_url . "admin/login");
} else if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_SESSION['admin']['msg'])) {
    $msg = unserialize($_SESSION['admin']['msg']);
} else {
    $msg = array();
}

require_once $app_path . 'views/admin/includes/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php
    require_once $app_path . 'views/admin/includes/header.php';
    ?>
    <?php
    require_once $app_path . 'views/admin/includes/sidebar.php';
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add Daily Activity
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="clear20"></div>
                        <form role="form"
                              class="col-md-offset-3 col-md-6 col-md-offset-2 col-md-8 col-md-offset-3 col-md-6"
                              enctype="multipart/form-data"
                              id="activity_validate" method="POST"
                              action="<?= $base_url ?>controllers/user/activities?action=add">
                            <?php require_once $app_path . 'views/errors.php'; ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Appointment Set In Diary</label>
                                    <input required min="0" type="number" class="form-control" name="d_appt_set_diary" id="d_appt_set_diary"
                                           placeholder="Enter Appointment Set In Diary">
                                </div>
                                <div class="form-group">
                                    <label for="email">Appt Sat</label>
                                    <input required min="0" type="number" class="form-control" name="d_appt"
                                              id="d_appt"
                                              placeholder="Enter Appt Sat" />
                                </div>
                                <div class="form-group">
                                    <label for="password">Sales Completed</label>
                                    <input required min="0" type="number"  class="form-control" name="d_sales"
                                              id="d_sales"
                                              placeholder="Enter Sales Completed" />
                                </div>
                                <div class="form-group">
                                    <label for="confirm_pass">Cold Calls Completed</label>
                                    <input required min="0" type="number"  class="form-control" name="d_cold_calls_completed"
                                              id="d_cold_calls_completed"
                                              placeholder="Enter Cold Calls Completed" />
                                </div>								
                                <div class="form-group">
                                    <label for="phone">New Appts Made</label>
                                    <input required min="0" type="number"  class="form-control" name="d_new_appt_made"
                                              id="d_new_appt_made"
                                              placeholder="Enter New Appts Made" />
                                </div>
								<div class="form-group">
                                    <label for="phone">Date of Activity</label>
                                    <input style="background:white;" required type="text" readonly value="<?= date('d-m-Y'); ?>"  class="form-control" name="d_entry_date"
                                              id="d_entry_date"
                                              placeholder="Enter Date of Activity" />
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>	
    <?php
    require_once $app_path . 'views/admin/includes/footer.php';
    require_once $app_path . 'views/admin/includes/foot.php';
    ?>
</body>
</html>
<?php
unset($_SESSION['admin']['msg']);
?>
<script>
var d = new Date();
var strDate = d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear() ;
$('#d_entry_date').datepicker({
	autocomplete: 'off',
	format: 'dd-mm-yyyy',
	endDate: strDate,
	
});
</script>