<header class="main-header">
    <a href="<?= $base_url ?>admin/dashboard" class="logo" style="background:#183d63;">
        <span class="logo-mini"><b><?= implode('', array_map(function($v) { return $v[0]; }, explode(' ', $site_name))); ?></b></span>
        <span class="logo-lg"><b><?= ucwords($site_name); ?></b></span>
    </a>
    <nav class="navbar navbar-static-top" style="background:#0C3158;">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
			   <!--<li class="dropdown messages-menu">
				<a href="<?= $base_url?>admin/mailbox/index">
				  <i class="fa fa-envelope-o"></i>
				  <span class="label label-danger"><?= count(Mailbox::all(['conditions'=>array('m_to_id'=>$_SESSION['admin']['id'],'m_status'=>0),'order'=>'m_id DESC']));?></span>
				</a>
			   </li> -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img style="object-fit: cover;" src="<?php 
						 if(empty($_SESSION['admin']['ext'])){	
                                       echo $base_url."assets/admin/Ionicons/png/512/android-contact.png";											
                     				}else{
							               echo $base_url.$_SESSION['admin']['img'].'original.'.$_SESSION['admin']['ext'];
									}
						
						?>" class="user-image"
                             alt="User Image">
                        <span class="hidden-xs"><?php echo $_SESSION['admin']['name']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header" style="background:#0C3158;">
                            <img style="object-fit: cover;" src="<?php
                                  if(empty($_SESSION['admin']['ext']) ){	
                                       echo $base_url."assets/admin/Ionicons/png/512/android-contact.png";											
                     				}else{
							               echo $base_url.$_SESSION['admin']['img'].'original.'.$_SESSION['admin']['ext'];
									}
							
							?>"
                                 class="img-circle" alt="User Image">

                            <p>
                             <?php echo $_SESSION['admin']['name']; ?>
                                <small>Member since <?php echo $_SESSION['admin']['created_at'];  ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= $base_url ?>admin/settings/index" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= $base_url ?>admin/logout?action=logout" class="btn btn-default btn-flat">Sign
                                    out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <!--<li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->