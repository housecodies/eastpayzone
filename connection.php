<?php
require_once __DIR__.'/vendor/autoload.php';

$cfg = ActiveRecord\Config::instance();
$cfg->set_connections(
    array(
        'development' => 'mysql://root:@localhost/payzone_reporting_system',
        'production' => 'mysql://payzone.housecodies.com:payzone.housecodies.com@localhost/westpz.housecodies.com'
    )
);
$cfg->set_default_connection('production');
?>