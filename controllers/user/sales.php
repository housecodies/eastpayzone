<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');
use Rakit\Validation\Validator;
if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}
if (isset($_GET['action']) && 'add' == $_GET['action']) {
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        's_daily_id' => 'required',
        's_customer_name' => 'required',
        's_service' => 'required',
        's_package' => 'required',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/sales/add");
    } else {
        if (!@$_POST['s_daily_id'] || 0 == $_POST['s_daily_id']) {
            $msg['errors'] = "Please Select The Activity Date";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/sales/add");
        }
        $data = Activities::all(['conditions' => ['d_id' => $_POST['s_daily_id']]]);
        $data1 = count(SaleDetail::all(['conditions' => ['s_daily_id' => $_POST['s_daily_id']]]));
        if ($data[0]->d_sales > $data1) {
            $last_id = SaleDetail::last();
            if ("" != $last_id) {
                $last_id = $last_id->s_id + 1;
            } else {
                $last_id = 1;
            }
            $saleDetail = new SaleDetail();
            $saleDetail->s_daily_id = $_POST['s_daily_id'];
            $saleDetail->s_customer_name = $_POST['s_customer_name'];
            $saleDetail->s_service = $_POST['s_service'];
            $saleDetail->s_package = $_POST['s_package'];
            $saleDetail->s_d_entry_date = $_POST['s_d_entry_date'];
            $saleDetail->s_user_id = $_SESSION['admin']['id'];
            $saleDetail->s_status = 1;
            $saleDetail->s_created_at = date('Y-m-d h:i:s');
            $saleDetail->s_updated_at = date('Y-m-d h:i:s');
            if ($saleDetail->save()) {
                $msg['success'] = "Sale Detail Added Successfully";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/sales/add");
        } else {
            $msg['errors'] = "You Have Exceeded The Limit of Sale Detail For This Activity";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/sales/add");
        }
    }
}
if (isset($_GET['action']) && 'edit' == $_GET['action']) {
    if (@$_GET['s_id']) {
        redirect($base_url . "admin/sales/edit?s_id=" . $_GET['s_id']);
    }
}
if (isset($_GET['action']) && 'update' == $_GET['action']) {
    if (@$_GET['s_id']) {
        $s_id = $_GET['s_id'];
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
            //'s_daily_id' => 'required',
            's_customer_name' => 'required',
            's_service' => 'required',
            's_package' => 'required',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/sales/edit?s_id=" . $s_id);
        } else {
            $check = SaleDetail::find(['conditions' => ['s_id' => $s_id]]);
            if ("" != $check) {
                $check->s_customer_name = $_POST['s_customer_name'];
                $check->s_service = $_POST['s_service'];
                $check->s_package = $_POST['s_package'];
                //$check->s_d_entry_date = $_POST['s_d_entry_date'];
                $check->s_updated_at = date('Y-m-d h:i:s');
                if ($check->save()) {
                    $msg['success'] = "Sale Updated Successfully";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/sales/edit?s_id=" . $s_id);
                } else {
                    $msg['errors'] = "There might be some errors, try again later.";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/sales/edit?s_id=" . $s_id);
                }
            } else {
                $msg['errors'] = "No Record Found.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/sales/edit?s_id=" . $s_id);
            }
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/sales/edit?s_id=" . $s_id);
    }
}
if (isset($_GET['action']) && 'delete' == $_GET['action']) {
    if (isset($_GET['s_id']) && "" != $_GET['s_id']) {
        $id = $_GET['s_id'];
        $check = SaleDetail::find(['conditions' => ['s_id' => $id]]);
        if ("" != $check) {
            $check = $check->delete();
            if ("" != $check) {
                $msg['success'] = "Record Deleted Successfully.";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
        } else {
            $msg['errors'] = "No Record Found.";
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
    }
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/sale-reports/index");
}
if (isset($_GET['action']) && 'getActi' == $_GET['action'] && isset($_GET['id'])) {
    $id = $_GET['id'];
    if (isset($id) && "" != $id) {
        $id = $_GET['id'];
        $check = Activities::all(['conditions' => ['d_user_id' => $id], 'order' => 'd_id DESC']);
        if (count($check) > 0) {
            foreach ($check as $c) {
                ?>
            <option value="<?php echo $c->d_id; ?>|<?php echo $c->d_entry_date; ?>"><?php echo "#" . $c->d_id . ") " . date('d-m-Y', strtotime($c->d_entry_date)); ?></option>
            <?php
}}
    }
}
if (isset($_GET['action']) && 'adminaddSale' == $_GET['action']) {
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        'user_id' => 'required',
        's_daily_id' => 'required',
        's_customer_name' => 'required',
        's_service' => 'required',
        's_package' => 'required',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/sale-reports/madd-sales");
    } else {
        if (!@$_POST['s_daily_id'] || 0 == $_POST['s_daily_id']) {
            $msg['errors'] = "Please Select The Activity Date";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/sale-reports/madd-sales");
        }
        $data = Activities::all(['conditions' => ['d_id' => $_POST['s_daily_id']]]);
        $data1 = count(SaleDetail::all(['conditions' => ['s_daily_id' => $_POST['s_daily_id']]]));
        if ($data[0]->d_sales > $data1) {
            $last_id = SaleDetail::last();
            if ("" != $last_id) {
                $last_id = $last_id->s_id + 1;
            } else {
                $last_id = 1;
            }
            $actData = explode('|', $_POST['s_daily_id']);
            $saleDetail = new SaleDetail();
            $saleDetail->s_daily_id = $actData[0];
            $saleDetail->s_customer_name = $_POST['s_customer_name'];
            $saleDetail->s_service = $_POST['s_service'];
            $saleDetail->s_package = $_POST['s_package'];
            $saleDetail->s_d_entry_date = $actData[1];
            $saleDetail->s_user_id = $_POST['user_id'];
            $saleDetail->s_status = 1;
            $saleDetail->s_created_at = date('Y-m-d h:i:s');
            $saleDetail->s_updated_at = date('Y-m-d h:i:s');
            if ($saleDetail->save()) {
                $msg['success'] = "Sale Detail Added Successfully";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/sale-reports/madd-sales");
        } else {
            $msg['errors'] = "You Have Exceeded The Limit of Sale Detail For This Activity";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/sale-reports/madd-sales");
        }
    }
}



?>