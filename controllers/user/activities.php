<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'add') {
	
	$timestamp = strtotime($_POST['d_entry_date']);
    $day = date('D', $timestamp);
	if($day=="Sun" || $day=="Sat"){
		$msg['errors'] = "Oops! Saturday And Sunday Are Holidays"; 
	    $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add"); 	
	}	
	
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        'd_appt_set_diary' => 'required',
		'd_appt' => 'required',
		'd_sales' => 'required',
		'd_cold_calls_completed' => 'required',
		'd_new_appt_made' => 'required',
		'd_entry_date' => 'required',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add");
    } else {
        $last_id = Activities::last();
        if ($last_id != "") {
            $last_id = $last_id->d_id + 1;
        } else {
            $last_id = 1;
        }
        $activity = new Activities();
		if(count(Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id'],'d_entry_date'=>date('Y-m-d',strtotime($_POST['d_entry_date']))),'order' => 'd_entry_date DESC']))>0){
		$msg['errors'] = "You Already Entered This Activity If You want to edit Please Contact Your Manager";
		$_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add");	
		}
        $activity->d_appt_set_diary = $_POST['d_appt_set_diary'];
        $activity->d_appt = $_POST['d_appt'];
        $activity->d_sales = $_POST['d_sales'];
        $activity->d_cold_calls_completed = $_POST['d_cold_calls_completed'];		
        $activity->d_new_appt_made = $_POST['d_new_appt_made'];
        $activity->d_entry_date = date('Y-m-d',strtotime($_POST['d_entry_date']));
		$activity->d_activity= $_POST['d_appt']+$_POST['d_new_appt_made'];
		if($_POST['d_appt_set_diary']!='0'){
		$activity->d_conversion_rate_set_sat=($_POST['d_appt']/$_POST['d_appt_set_diary'])*100;
		}else{
		$activity->d_conversion_rate_set_sat=0;	
		}
		if($_POST['d_appt']!='0'){
		$activity->d_conversion_rate_set_sold=($_POST['d_sales']/$_POST['d_appt'])*100;
		}else{
		$activity->d_conversion_rate_set_sold=0;	
		}
        $activity->d_status = 1;
		$activity->d_sellday=1;
		$activity->d_user_id = $_SESSION['admin']['id'];
        $activity->d_created_at = date('Y-m-d h:i:s');
        $activity->d_updated_at = date('Y-m-d h:i:s');
		
        if ($activity->save()) {
            $msg['success'] = "Activity Created Successfully";
        } else {
            $msg['errors'] = "There might be some errors, try again later.";
        }
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/add");
    }
}


if (isset($_GET['action']) && $_GET['action'] == 'edit') {
	if(@$_GET['d_id']){
		redirect($base_url . "admin/activities/edit?d_id=".$_GET['d_id']); 
	}
}


if (isset($_GET['action']) && $_GET['action'] == 'update') {
    if (isset($_GET['d_id']) && $_GET['d_id'] != "") {
		$id=$_GET['d_id'];
	  if($_POST['d_sales']>=SaleDetail::count(['conditions'=>array('s_daily_id'=>$id)])){
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
        'd_appt_set_diary' => 'required',
		'd_appt' => 'required',
		'd_sales' => 'required',
		'd_cold_calls_completed' => 'required',
		'd_new_appt_made' => 'required',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/activities/edit?d_id=".$id);
        } else {
            $id = $_GET['d_id'];
            $check = Activities::find(['conditions' => ['d_id' => $id]]);
            if ($check != "") {
				$check->d_appt_set_diary = $_POST['d_appt_set_diary'];
				$check->d_appt = $_POST['d_appt'];
				$check->d_sales = $_POST['d_sales'];
				$check->d_cold_calls_completed = $_POST['d_cold_calls_completed'];		
				$check->d_new_appt_made = $_POST['d_new_appt_made'];
				$check->d_activity= $_POST['d_appt']+$_POST['d_new_appt_made'];
				if($_POST['d_appt_set_diary']!="0"){
				$check->d_conversion_rate_set_sat=($_POST['d_appt']/$_POST['d_appt_set_diary'])*100;
				}else{
				$check->d_conversion_rate_set_sat=0;	
				}
				if($_POST['d_appt']!="0"){
				$check->d_conversion_rate_set_sold=($_POST['d_sales']/$_POST['d_appt'])*100;
				}else{
				$check->d_conversion_rate_set_sold=0;	
				}
				$check->d_updated_at = date('Y-m-d h:i:s');				

                if ($check->save()) {
                    $msg['success'] = "Activity Updated Successfully";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/activities/edit?d_id=".$id);
				}	
                 else {
                    $msg['errors'] = "There might be some errors, try again later.";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/activities/edit?d_id=".$id);
                }
            } else {
                $msg['errors'] = "No Record Found.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/activities/edit?d_id=".$id);
            }
        }
	  }else{
		        $msg['errors'] = "Please Amend Sale Activity First.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/activities/edit?d_id=".$id);
	  }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/settings/index");
    }
}



if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    if (isset($_GET['d_id']) && $_GET['d_id'] != "") {
        $id = $_GET['d_id'];
        $check = Activities::find(['conditions' => ['d_id' => $id]]);
        if ($check != "") {
			$saleCheck = SaleDetail::all(['conditions' => ['s_daily_id' => $check->d_id]]);
			if(count($saleCheck)){
			foreach($saleCheck as $c){
				$c = $c->delete();
			}
			}
            $check = $check->delete();
            if ($check != "") {
                $msg['success'] = "Record Deleted Successfully.";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
        } else {
            $msg['errors'] = "No Record Found.";
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
    }
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/activities/manage");
}






/* Admin Add activity method */

if (isset($_GET['action']) && $_GET['action'] == 'adminaddact') {
	$timestamp = strtotime($_POST['d_entry_date']);
    $day = date('D', $timestamp);
	if($day=="Sun" || $day=="Sat"){
		$msg['errors'] = "Oops! Saturday And Sunday Are Holidays"; 
	    $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/m-adminaddact"); 	
	}	
	
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        'd_appt_set_diary' => 'required',
		'd_appt' => 'required',
		'd_sales' => 'required',
		'd_cold_calls_completed' => 'required',
		'd_new_appt_made' => 'required',
		'd_entry_date' => 'required',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/m-adminaddact");
    } else {
        $last_id = Activities::last();
        if ($last_id != "") {
            $last_id = $last_id->d_id + 1;
        } else {
            $last_id = 1;
        }
        $activity = new Activities();
		if(count(Activities::all(['conditions'=>array('d_user_id'=>$_SESSION['admin']['id'],'d_entry_date'=>date('Y-m-d',strtotime($_POST['d_entry_date']))),'order' => 'd_entry_date DESC']))>0){
		$msg['errors'] = "You Already Entered This Activity, Please Edit This From Edit Module";
		$_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/m-adminaddact");	
		}
        $activity->d_appt_set_diary = $_POST['d_appt_set_diary'];
        $activity->d_appt = $_POST['d_appt'];
        $activity->d_sales = $_POST['d_sales'];
        $activity->d_cold_calls_completed = $_POST['d_cold_calls_completed'];		
        $activity->d_new_appt_made = $_POST['d_new_appt_made'];
        $activity->d_entry_date = date('Y-m-d',strtotime($_POST['d_entry_date']));
		$activity->d_activity= $_POST['d_appt']+$_POST['d_new_appt_made'];
		if($_POST['d_appt_set_diary']!='0'){
		$activity->d_conversion_rate_set_sat=($_POST['d_appt']/$_POST['d_appt_set_diary'])*100;
		}else{
		$activity->d_conversion_rate_set_sat=0;	
		}
		if($_POST['d_appt']!='0'){
		$activity->d_conversion_rate_set_sold=($_POST['d_sales']/$_POST['d_appt'])*100;
		}else{
		$activity->d_conversion_rate_set_sold=0;	
		}
        $activity->d_status = 1;
		$activity->d_sellday=1;
		$activity->d_user_id = $_POST['user_id'];
        $activity->d_created_at = date('Y-m-d h:i:s');
        $activity->d_updated_at = date('Y-m-d h:i:s');
		
        if ($activity->save()) {
            $msg['success'] = "Activity Created Successfully";
        } else {
            $msg['errors'] = "There might be some errors, try again later.";
        }
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/activities/m-adminaddact");
    }
}


?>