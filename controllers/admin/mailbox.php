<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'add' && isset($_GET['type']) && !@$_GET['dash']) {
    $validator = new Validator;
    $validation = $validator->make($_POST + $_FILES, [
        'm_id' => 'required',
		'm_sub' => 'required',
		'm_msg' => 'required',
    ]);
	$validation->setAliases([
	'm_id' => 'Email',
	'm_sub' => 'Subject',
	'm_msg' => 'Message'
    ]);
	$validation->validate();
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/mailbox/add");
    } else {
	      if(!@$_POST['m_id']){
          $msg['errors'] = "Please Select The Emails";
          $_SESSION['admin']['msg'] = serialize($msg);
          redirect($base_url . "admin/mailbox/add"); 	
        }   		      				 		
        if($_GET['type']==0){
			$ids=$_POST['m_id'];
			foreach($ids as $id){
				        $Mailbox = new Mailbox();
						$Mailbox->m_from_id = $_SESSION['admin']['id'];
						$Mailbox->m_to_id = $id;
						$Mailbox->m_msg = $_POST['m_msg'];
						$Mailbox->m_sub = $_POST['m_sub'];
						$Mailbox->m_status = 0;
						$Mailbox->m_created_at = date('Y-m-d h:i:s');
						$Mailbox->m_updated_at = date('Y-m-d h:i:s');
				        if ($Mailbox->save()) {
							 $user=User::all(['conditions'=>array('user_id'=>$id)]);
							 $from['name']=$_SESSION['admin']['name'];
							 $from['email']=$_SESSION['admin']['email'];
							 do_email($user[0]->user_email,$from,$_POST['m_sub'],$_POST['m_msg']);
                             $msg['success'] = "Message Sent Successfully";
                            } else {
                             $msg['errors'] = "There might be some errors, try again later.";
                            }
                            
		 			 }
			 $_SESSION['admin']['msg'] = serialize($msg);
             redirect($base_url . "admin/mailbox/add");		 

		}else{
			
			$ids=$_POST['m_id'];
			foreach($ids as $id){
				        $Mailbox = new Mailbox();
						$Mailbox->m_from_id = $_SESSION['admin']['id'];
						$Mailbox->m_to_id = $id;
						$Mailbox->m_msg = $_POST['m_msg'];
						$Mailbox->m_sub = $_POST['m_sub'];
						$Mailbox->m_status = 0;
						$Mailbox->m_created_at = date('Y-m-d h:i:s');
						$Mailbox->m_updated_at = date('Y-m-d h:i:s');
				        if ($Mailbox->save()) {
                             $msg['success'] = "Message Sent Successfully";
							 $user=User::all(['conditions'=>array('user_id'=>$id)]);
							 $from['name']=$_SESSION['admin']['name'];
							 $from['email']=$_SESSION['admin']['email'];
							 do_email($user[0]->user_email,$from,$_POST['m_sub'],$_POST['m_msg']);
                            } else {
                             $msg['errors'] = "There might be some errors, try again later.";
                            }
                            
		 			 }
			 $_SESSION['admin']['msg'] = serialize($msg);
             redirect($base_url . "admin/mailbox/add");		 
			
		}	


	   
	   
	
	}
}

if (isset($_GET['action']) && $_GET['action'] == 'add' && isset($_GET['type']) && @$_GET['dash'] && $_GET['dash']==1) {
    $validator = new Validator;
    $validation = $validator->make($_POST + $_FILES, [
        'm_id' => 'required',
		'm_sub' => 'required',
		'm_msg' => 'required',
    ]);
	$validation->setAliases([
	'm_id' => 'Email',
	'm_sub' => 'Subject',
	'm_msg' => 'Message'
    ]);
	$validation->validate();
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/dashboard");
    } else {
	      if(!@$_POST['m_id']){
          $msg['errors'] = "Please Select The Emails";
          $_SESSION['admin']['msg'] = serialize($msg);
          redirect($base_url . "admin/dashboard"); 	
        }   		      				 		
        if($_GET['type']==0){
			$ids=$_POST['m_id'];
			foreach($ids as $id){
				        $Mailbox = new Mailbox();
						$Mailbox->m_from_id = $_SESSION['admin']['id'];
						$Mailbox->m_to_id = $id;
						$Mailbox->m_msg = $_POST['m_msg'];
						$Mailbox->m_sub = $_POST['m_sub'];
						$Mailbox->m_status = 0;
						$Mailbox->m_created_at = date('Y-m-d h:i:s');
						$Mailbox->m_updated_at = date('Y-m-d h:i:s');
				        if ($Mailbox->save()) {
							 $user=User::all(['conditions'=>array('user_id'=>$id)]);
							 $from['name']=$_SESSION['admin']['name'];
							 $from['email']=$_SESSION['admin']['email'];
							 do_email($user[0]->user_email,$from,$_POST['m_sub'],$_POST['m_msg']);
                             $msg['success'] = "Message Sent Successfully";
                            } else {
                             $msg['errors'] = "There might be some errors, try again later.";
                            }
                            
		 			 }
			 $_SESSION['admin']['msg'] = serialize($msg);
             redirect($base_url . "admin/dashboard");		 

		}else{
			
			$ids=$_POST['m_id'];
			foreach($ids as $id){
				        $Mailbox = new Mailbox();
						$Mailbox->m_from_id = $_SESSION['admin']['id'];
						$Mailbox->m_to_id = $id;
						$Mailbox->m_msg = $_POST['m_msg'];
						$Mailbox->m_sub = $_POST['m_sub'];
						$Mailbox->m_status = 0;
						$Mailbox->m_created_at = date('Y-m-d h:i:s');
						$Mailbox->m_updated_at = date('Y-m-d h:i:s');
				        if ($Mailbox->save()) {
                             $msg['success'] = "Message Sent Successfully";
							 $user=User::all(['conditions'=>array('user_id'=>$id)]);
							 $from['name']=$_SESSION['admin']['name'];
							 $from['email']=$_SESSION['admin']['email'];
							 do_email($user[0]->user_email,$from,$_POST['m_sub'],$_POST['m_msg']);
                            } else {
                             $msg['errors'] = "There might be some errors, try again later.";
                            }
                            
		 			 }
			 $_SESSION['admin']['msg'] = serialize($msg);
             redirect($base_url . "admin/dashboard");		 
			
		}	


	   
	   
	
	}
}

?>