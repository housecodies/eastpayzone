<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'add') {
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        'name' => 'required',
		'email' => 'required|email',
		'password' => 'required|min:6',
		'designation' => 'required',
        'image' => 'uploaded_file:0,5M,png,jpeg,jpg,gif',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/users/add");
    } else {
        $last_id = User::last();
        if ($last_id != "") {
            $last_id = $last_id->user_id + 1;
        } else {
            $last_id = 1;
        }
        $sizing = array();
        $image = upload_image('image', $app_path, 'uploads/user_images/' . $last_id, $sizing);
        $user = new User();
        $user->user_name = $_POST['name'];
        $user->user_pass = md5($_POST['password']);
        $user->user_email = $_POST['email'];
        $user->user_phone = $_POST['phone'];		
        $user->user_image = $image['file_name'];
        $user->user_desg = $_POST['designation'];
        $user->user_image_type = $image['ext'];
		if(@$_POST['designation'] && $_POST['designation']==1){
		$user->user_type= $_POST['usertype'];	
		}else{
		$user->user_type=0;	
		}
        $user->user_status = 1;
        $user->user_created_at = date('Y-m-d h:i:s');
        $user->user_updated_at = date('Y-m-d h:i:s');
        if ($user->save()) {
            $msg['success'] = "User Created Successfully";
        } else {
            $msg['errors'] = "There might be some errors, try again later.";
        }
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/users/add");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    if (isset($_GET['user_id']) && $_GET['user_id'] != "") {
        $id = decode_url($_GET['user_id']);
        $check = User::find(['conditions' => ['user_id' => $id]]);
        if ($check != "") {
            $check = $check->delete();
            if ($check != "") {
                $msg['success'] = "Record Deleted Successfully.";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
        } else {
            $msg['errors'] = "No Record Found.";
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
    }
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/users/index");
} else if (isset($_GET['action']) && $_GET['action'] == 'edit') {
    if (isset($_GET['user_id']) && $_GET['user_id'] != "") {
        redirect($base_url . "admin/users/edit?user_id=" . $_GET['user_id']);
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/users/index");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'update') {
    if (isset($_GET['user_id']) && $_GET['user_id'] != "") {
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
        'name' => 'required',
		'email' => 'required|email',
		'password'=>'min:6',
		'designation' => 'required',
        'image' => 'uploaded_file:0,5M,png,jpeg,jpg,gif',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/users/edit?user_id=" . $_GET['user_id']);
        } else {
            $id = decode_url($_GET['user_id']);
            $check = User::find(['conditions' => ['user_id' => $id]]);
            if ($check != "") {
                if ($_FILES['image']['name'] != "" && isset($_FILES['image']['name'])) {
                    $sizing = array();
                    $image = upload_image('image', $app_path, 'uploads/user_images/' . $id, $sizing);
			        if($_SESSION['admin']['id']==$id){
                    $_SESSION['admin']['img']= $file_name = $image['file_name'];
					$_SESSION['admin']['ext']= $ext = $image['ext'];
					}else{$file_name = $image['file_name'];$ext = $image['ext'];}
                    
                } else {
                    $file_name = $check->user_image;
                    $ext = $check->user_image_type;
                }
				$check->user_name = $_POST['name'];
				if(@$_POST['password'])
				$check->user_pass = md5($_POST['password']);
			    if(@$_POST['user_pror'])
				$check->user_pror = $_POST['user_pror'];	
				$check->user_email = $_POST['email'];
				$check->user_phone = $_POST['phone'];		
				$check->user_image = $file_name;
				$check->user_desg = $_POST['designation'];
				$check->user_image_type = $ext;
				$check->user_updated_at = date('Y-m-d h:i:s');
				if(@$_POST['usertype']){
		         $check->user_type= $_POST['usertype'];	
		        }else{
		             $check->user_type=0;	
		        }
                if ($check->save()) {
                    $msg['success'] = "Record Updated Successfully";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/users/edit?user_id=" . $_GET['user_id']);
                } else {
                    $msg['errors'] = "There might be some errors, try again later.";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/users/edit?user_id=" . $_GET['user	_id']);
                }
            } else {
                $msg['errors'] = "No Record Found.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/users/index");
            }
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/users/index");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'status_off') {
    if (isset($_GET['user_id']) && $_GET['user_id'] != "") {
        $id = decode_url($_GET['user_id']);
        $check = User::find(['conditions' => ['user_id' => $id]]);
        if ($check != "") {
            $check->user_status = 0;
            $check->user_updated_at = date('Y-m-d h:i:s');
            if ($check->save()) {
				if($_SESSION['admin']['id']==$id){
					unset($_SESSION['admin']);
                   $msg['success'] = "Record Has Been Deactivated.";
                   $_SESSION['admin']['msg'] = serialize($msg);
                   redirect($base_url . "admin/login");
				}
                unset($_SESSION['user_id']);
                $msg['success'] = "Record Updated Successfully";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/users/index");
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/users/index");
            }
        } else {
            $msg['errors'] = "No Record Found.";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/users/index");
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/users/index");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'status_on') {
    if (isset($_GET['user_id']) && $_GET['user_id'] != "") {
        $id = decode_url($_GET['user_id']);
        $check = User::find(['conditions' => ['user_id' => $id]]);
        if ($check != "") {
            $check->user_status = 1;
            $check->user_updated_at = date('Y-m-d h:i:s');
            if ($check->save()) {
                unset($_SESSION['user_id']);
                $msg['success'] = "Record Updated Successfully";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/users/index");
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/users/index");
            }
        } else {
            $msg['errors'] = "No Record Found.";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/users/index");
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/users/index");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'email_validation') {
	$count = 0;
	if(isset($_POST['user_id']) && $_POST['user_id'] != ""){
		if(isset($_POST['email']) && $_POST['email'] != "") {
			$count = User::count(['conditions' => ' user_email = "'. $_POST['email'] . '" AND user_id != "' . $_POST['user_id'] . '"']);
		}
	}else{
		if(isset($_POST['email']) && $_POST['email'] != "") {
			$count = User::count(['conditions' => ['user_email' => $_POST['email']]]);
		}
	}
	if($count == 0){
		echo 'success';die();   
	}else{
		echo 'errors';die();
	}
} else {
    redirect($base_url . "admin/dashboard");
}
?>