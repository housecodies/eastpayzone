<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'add' && isset($_GET['cps'])) {
    if (isset($_POST['cps_formula']) && $_POST['cps_formula'] != "") {
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
		'cps_formula'=>'required',
		'cps_coldcall'=>'required',
		'cps_ref'=>'required',
		'cps_tele'=>'required',
		'cpsdate'=>'required',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/leaderboard/index");
        } else {
				$cps = new Cps();
				$cps->cps_mpl_criteria = $_POST['cps_formula'];
				$cps->cps_mpl_cold_call = $_POST['cps_coldcall'];
				$cps->cps_mpl_reff = $_POST['cps_ref'];
				$cps->cps_mpl_tele = $_POST['cps_tele'];
				$cps->cps_mpl_date = date('Y-m-d',strtotime($_POST['cpsdate']));
				$cps->cps_mpl_status = 1;
				$cps->cps_mpl_created_at = date('Y-m-d h:i:s');
				$cps->cps_mpl_updated_at = date('Y-m-d h:i:s');
				if ($cps->save()) {
					$msg['success'] = "CPS Calculations Created Successfully !";
				} else {
					$msg['errors'] = "There might be some errors, try again later.";
				}
				$_SESSION['admin']['msg'] = serialize($msg);
				redirect($base_url . "admin/leaderboard/index");
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/leaderboard/index");
    }
}


if (isset($_GET['action']) && $_GET['action'] == 'add' && isset($_GET['ccps'])) {
    if (isset($_POST['ccps_formula']) && $_POST['ccps_formula'] != "") {
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
		'ccps_formula'=>'required',
		'ccps_coldcall'=>'required',
		'ccps_ref'=>'required',
		'ccps_tele'=>'required',
		'ccpsdate'=>'required',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/leaderboard/index");
        } else {
				$ccps = new Ccps();
				$ccps->ccps_mpl_criteria = $_POST['ccps_formula'];
				$ccps->ccps_mpl_cold_call = $_POST['ccps_coldcall'];
				$ccps->ccps_mpl_reff = $_POST['ccps_ref'];
				$ccps->ccps_mpl_tele = $_POST['ccps_tele'];
				$ccps->ccps_mpl_date = date('Y-m-d',strtotime($_POST['ccpsdate']));
				$ccps->ccps_mpl_status = 1;
				$ccps->ccps_mpl_created_at = date('Y-m-d h:i:s');
				$ccps->ccps_mpl_updated_at = date('Y-m-d h:i:s');
				if ($ccps->save()) {
					$msg['success'] = "CC CPS Calculations Created Successfully !";
				} else {
					$msg['errors'] = "There might be some errors, try again later.";
				}
				$_SESSION['admin']['msg'] = serialize($msg);
				redirect($base_url . "admin/leaderboard/index");
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/leaderboard/index");
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'delete' && isset($_GET['cps'])) {
    if (isset($_GET['cps']) && $_GET['cps'] != "") {
        $id = decode_url($_GET['cps_mpl_id']);
        $check = Cps::find(['conditions' => ['cps_mpl_id' => $id]]);
        if ($check != "") {
            $check = $check->delete();
            if ($check != "") {
                $msg['success'] = "Record Deleted Successfully.";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
        } else {
            $msg['errors'] = "No Record Found.";
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
    }
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/leaderboard/managecps");
}

if (isset($_GET['action']) && $_GET['action'] == 'delete' && isset($_GET['ccps'])) {
    if (isset($_GET['ccps']) && $_GET['ccps'] != "") {
        $id = decode_url($_GET['ccps_mpl_id']);
        $check = Ccps::find(['conditions' => ['ccps_mpl_id' => $id]]);
        if ($check != "") {
            $check = $check->delete();
            if ($check != "") {
                $msg['success'] = "Record Deleted Successfully.";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
        } else {
            $msg['errors'] = "No Record Found.";
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
    }
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/leaderboard/manageccps");
}


if (isset($_GET['action']) && $_GET['action'] == 'edit' && isset($_GET['cps'])) {
    if (isset($_GET['cps']) && $_GET['cps'] != "") {
		$id=decode_url($_GET['cps_mpl_id']);
        redirect($base_url . "admin/leaderboard/editcps?cps_mpl_id=".$id);
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/leaderboard/managecps");
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'edit' && isset($_GET['ccps'])) {
    if (isset($_GET['ccps']) && $_GET['ccps'] != "") {
		$id=decode_url($_GET['ccps_mpl_id']);
        redirect($base_url . "admin/leaderboard/editccps?ccps_mpl_id=".$id);
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/leaderboard/manageccps");
    }
}







if (isset($_GET['action']) && $_GET['action'] == 'update' && isset($_GET['cps'])) {
    if (isset($_GET['cps_mpl_id']) && $_GET['cps_mpl_id'] != "") {
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
		'cps_formula'=>'required',
		'cps_coldcall'=>'required',
		'cps_ref'=>'required',
		'cps_tele'=>'required',
		'cpsdate'=>'required',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/leaderboard/editcps?cps_mpl_id=" . $_GET['cps_mpl_id']);
        } else {
            $id = $_GET['cps_mpl_id'];
            $check = Cps::find(['conditions' => ['cps_mpl_id' => $id]]);
            if ($check != "") {
				$check->cps_mpl_criteria = $_POST['cps_formula'];
				$check->cps_mpl_cold_call = $_POST['cps_coldcall'];
				$check->cps_mpl_reff = $_POST['cps_ref'];
				$check->cps_mpl_tele = $_POST['cps_tele'];
				$check->cps_mpl_date = date('Y-m-d',strtotime($_POST['cpsdate']));
				$check->cps_mpl_status = 1;
				$check->cps_mpl_created_at = date('Y-m-d h:i:s');
				$check->cps_mpl_updated_at = date('Y-m-d h:i:s');
                if ($check->save()) {
                    $msg['success'] = "Calculations Updated Successfully";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/leaderboard/editcps?cps_mpl_id=" . $_GET['cps_mpl_id']);
                } else {
                    $msg['errors'] = "There might be some errors, try again later.";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/leaderboard/editcps?cps_mpl_id=" . $_GET['cps_mpl_id']);
                }
            } else {
                $msg['errors'] = "No Record Found.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/leaderboard/managecps");
            }
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/leaderboard/managecps");
    }
}



if (isset($_GET['action']) && $_GET['action'] == 'update' && isset($_GET['ccps'])) {
    if (isset($_GET['ccps_mpl_id']) && $_GET['ccps_mpl_id'] != "") {
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
		'ccps_formula'=>'required',
		'ccps_coldcall'=>'required',
		'ccps_ref'=>'required',
		'ccps_tele'=>'required',
		'ccpsdate'=>'required',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/leaderboard/editcps?ccps_mpl_id=" . $_GET['ccps_mpl_id']);
        } else {
            $id = $_GET['ccps_mpl_id'];
            $check = Ccps::find(['conditions' => ['ccps_mpl_id' => $id]]);
            if ($check != "") {
				$check->ccps_mpl_criteria = $_POST['ccps_formula'];
				$check->ccps_mpl_cold_call = $_POST['ccps_coldcall'];
				$check->ccps_mpl_reff = $_POST['ccps_ref'];
				$check->ccps_mpl_tele = $_POST['ccps_tele'];
				$check->ccps_mpl_date = date('Y-m-d',strtotime($_POST['ccpsdate']));
				$check->ccps_mpl_status = 1;
				$check->ccps_mpl_created_at = date('Y-m-d h:i:s');
				$check->ccps_mpl_updated_at = date('Y-m-d h:i:s');
                if ($check->save()) {
                    $msg['success'] = "Calculations Updated Successfully";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/leaderboard/editccps?ccps_mpl_id=" . $_GET['ccps_mpl_id']);
                } else {
                    $msg['errors'] = "There might be some errors, try again later.";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/leaderboard/editccps?ccps_mpl_id=" . $_GET['ccps_mpl_id']);
                }
            } else {
                $msg['errors'] = "No Record Found.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/leaderboard/manageccps");
            }
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/leaderboard/manageccps");
    }
}

?>