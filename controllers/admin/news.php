<?php
session_start();
require_once '../config.php';
require_once $app_path . 'helpers.php';
require_once $app_path . 'connection.php';
$cfg->set_model_directory($app_path . 'models');

use Rakit\Validation\Validator;

if (admin_logged_in($_SESSION) == -1 || admin_logged_in($_SESSION) == -2) {
    unset($_SESSION['admin']);
    $msg['errors'] = 'Your session cookie was expired. Please log in again.';
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/login");
}

if (isset($_GET['action']) && $_GET['action'] == 'add') {
    $validator = new Validator;
    $validation = $validator->validate($_POST + $_FILES, [
        'title' => 'required',
        'description' => 'required|min:10',
        'image' => 'required|uploaded_file:0,5M,png,jpeg,jpg,gif',
    ]);
    if ($validation->fails()) {
        $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/news/add");
    } else {
        $last_id = News::last();
        if ($last_id != "") {
            $last_id = $last_id->news_id + 1;
        } else {
            $last_id = 1;
        }
        $sizing = array(
            'small' => ['width' => 273, 'height' => 193],
            'medium' => ['width' => 340, 'height' => 213],
            'high' => ['width' => 768, 'height' => 329],
            'banner' => ['width' => 950, 'height' => 456],
        );
        $image = upload_image('image', $app_path, 'uploads/news_images/' . $last_id, $sizing);
        $news = new News();
        $news->news_title = $_POST['title'];
        $news->news_slug = create_slug($_POST['title']);
        $news->news_description = $_POST['description'];
        $news->news_image = $image['file_name'];
        $news->news_image_type = $image['ext'];
        $news->news_status = 1;
        $news->news_created = date('Y-m-d h:i:s');
        $news->news_updated = date('Y-m-d h:i:s');
        if ($news->save()) {
            $msg['success'] = "News Created Successfully";
        } else {
            $msg['errors'] = "There might be some errors, try again later.";
        }
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/news/add");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    if (isset($_GET['news_id']) && $_GET['news_id'] != "") {
        $id = decode_url($_GET['news_id']);
        $check = News::find(['conditions' => ['news_id' => $id]]);
        if ($check != "") {
            $check = $check->delete();
            if ($check != "") {
                $msg['success'] = "Record Deleted Successfully.";
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
            }
        } else {
            $msg['errors'] = "No Record Found.";
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
    }
    $_SESSION['admin']['msg'] = serialize($msg);
    redirect($base_url . "admin/news/index");
} else if (isset($_GET['action']) && $_GET['action'] == 'edit') {
    if (isset($_GET['news_id']) && $_GET['news_id'] != "") {
        redirect($base_url . "admin/news/edit?news_id=" . $_GET['news_id']);
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/news/index");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'update') {
    if (isset($_GET['news_id']) && $_GET['news_id'] != "") {
        $validator = new Validator;
        $validation = $validator->validate($_POST + $_FILES, [
            'title' => 'required',
            'description' => 'required|min:10',
            'image' => 'uploaded_file:0,5M,png,jpeg,jpg,gif',
        ]);
        if ($validation->fails()) {
            $msg['errors'] = implode('<br>', $validation->errors()->firstOfAll());
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/news/edit?news_id=" . $_GET['news_id']);
        } else {
            $id = decode_url($_GET['news_id']);
            $check = News::find(['conditions' => ['news_id' => $id]]);
            if ($check != "") {
                if ($_FILES['image']['name'] != "" && isset($_FILES['image']['name'])) {
                    $sizing = array(
                        'small' => ['width' => 273, 'height' => 193],
                        'medium' => ['width' => 340, 'height' => 213],
                        'high' => ['width' => 768, 'height' => 329],
                        'banner' => ['width' => 950, 'height' => 456],
                    );
                    $image = upload_image('image', $app_path, 'uploads/news_images/' . $id, $sizing);
                    $file_name = $image['file_name'];
                    $ext = $image['ext'];
                } else {
                    $file_name = $check->news_image;
                    $ext = $check->news_image_type;
                }
                $check->news_title = $_POST['title'];
                $check->news_slug = create_slug($_POST['title']);
                $check->news_description = $_POST['description'];
                $check->news_image = $file_name;
                $check->news_image_type = $ext;
                $check->news_updated = date('Y-m-d h:i:s');
                if ($check->save()) {
                    $msg['success'] = "News Updated Successfully";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/news/edit?news_id=" . $_GET['news_id']);
                } else {
                    $msg['errors'] = "There might be some errors, try again later.";
                    $_SESSION['admin']['msg'] = serialize($msg);
                    redirect($base_url . "admin/news/edit?news_id=" . $_GET['news_id']);
                }
            } else {
                $msg['errors'] = "No Record Found.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/news/index");
            }
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/news/index");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'status_off') {
    if (isset($_GET['news_id']) && $_GET['news_id'] != "") {
        $id = decode_url($_GET['news_id']);
        $check = News::find(['conditions' => ['news_id' => $id]]);
        if ($check != "") {
            $check->news_status = 0;
            $check->news_updated = date('Y-m-d h:i:s');
            if ($check->save()) {
                unset($_SESSION['news_id']);
                $msg['success'] = "News Updated Successfully";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/news/index");
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/news/index");
            }
        } else {
            $msg['errors'] = "No Record Found.";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/news/index");
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/news/index");
    }
} else if (isset($_GET['action']) && $_GET['action'] == 'status_on') {
    if (isset($_GET['news_id']) && $_GET['news_id'] != "") {
        $id = decode_url($_GET['news_id']);
        $check = News::find(['conditions' => ['news_id' => $id]]);
        if ($check != "") {
            $check->news_status = 1;
            $check->news_updated = date('Y-m-d h:i:s');
            if ($check->save()) {
                unset($_SESSION['news_id']);
                $msg['success'] = "News Updated Successfully";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/news/index");
            } else {
                $msg['errors'] = "There might be some errors, try again later.";
                $_SESSION['admin']['msg'] = serialize($msg);
                redirect($base_url . "admin/news/index");
            }
        } else {
            $msg['errors'] = "No Record Found.";
            $_SESSION['admin']['msg'] = serialize($msg);
            redirect($base_url . "admin/news/index");
        }
    } else {
        $msg['errors'] = "There might be some errors, try again later.";
        $_SESSION['admin']['msg'] = serialize($msg);
        redirect($base_url . "admin/news/index");
    }
} else {
    redirect($base_url . "admin/dashboard");
}
?>