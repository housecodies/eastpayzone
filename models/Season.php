<?php

class Season extends ActiveRecord\Model
{
    public static $primary_key = 's_id';
    public static $table_name = 'seasons';
    
   static $has_many = array(
        array('season_to_comp', 'class_name' => 'Compitition', 'foreign_key' => 'c_id', 'primary_key' => 's_id'),
    );
}

